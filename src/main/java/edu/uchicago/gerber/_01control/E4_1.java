package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class E4_1 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        //a) sum of even numbers between 2 and 100
        int sumEvenA = 0;
        for (int i = 1; i < 51 ; i++) {
            sumEvenA+=2*i;
        }
        System.out.println("a) The sum of even numbers between 2 and 100 is " + sumEvenA);
        
        //b) sum of squares between 1 and 100
        int sumSqB = 0;
        int toSquare = 0;
        while(toSquare*toSquare<=100){
            sumSqB += toSquare*toSquare;
            toSquare++;
        }

        System.out.println("\nb) The sum of squares between 1 and 100 is " + sumSqB);
        
        //c) all powers of 2 from 20 to 220, smallest is 32 2^5
        System.out.println("\nc) The powers of 2 from 20 to 220 are:");
        int power = 5;
        int powOf2 = (int) Math.pow(2,power);
        do {
            System.out.println(powOf2);
            power++;
            powOf2= (int) Math.pow(2,power);
        }
        while (powOf2<220);

        //d) sum of odd numbers between a and b (inclusive)
        System.out.println("\nd) Please input 2 integers: ");
        int in1 = in.nextInt();
        int in2 = in.nextInt();
        int a = Math.min(in1, in2);
        int b = Math.max(in1, in2);
        //if smaller number is even, add 1 to start loop at an odd number
        if(a%2==0){
            a = a+1;
        }
        int sumOdd = 0;
        //loop through smallest number up to largest number + 1 so the sum can be inclusive if b is odd
        for (int i = a ; i <b+1 ; i+=2) {
            sumOdd += i;
        }
        System.out.println("The sum of all odd numbers between your two inputs is " + sumOdd);

        //e) sum of all odd digits of an input
        System.out.println("\ne) Please input a number: ");
        int inputE = in.nextInt();
        int sumE = 0;
        double pow10 = 0;
        //to find each digit, find remainder as the number is divided by increasing powers of 10 and divide by one less power of 10
        while(inputE/Math.pow(10,pow10)>0){
            int digit = (int) ((inputE%Math.pow(10,pow10+1))/Math.pow(10,pow10));
            //only add digit to the sum if the digit is odd
            if(digit%2==1){
                sumE+=digit;
            }
            pow10++;
        }
        System.out.println("The sum of the odd digits in your number is " + sumE);

    }
}
/*
Write programs with loops that compute
			a.The sum of all even numbers between 2 and 100 (inclusive).
			b.The sum of all squares between 1 and 100 (inclusive).
			c.All powers of 2 from 20 up to 220.
			d.The sum of all odd numbers between a and b (inclusive), where a and b are inputs.
			e.The sum of all odd digits of an input. (For example, if the input is 32677, the sum would be 3 + 7 + 7 = 17.)
 */
