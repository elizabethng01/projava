package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class P3_13 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a positive integer: ");
        int input = in.nextInt();
        if(input>3999 || input<0){
            System.out.println("Your number cannot be converted into a Roman numeral. Please enter a valid positive integer: ");
            input = in.nextInt();
        }
        String romanNum = "";
        //locate digit at each place value
        int thousands = input/1000;
        int hundreds = (input%1000)/100;
        int tens = (input%100)/10;
        int ones = input%10;

        //if the input is greater than 1000, add as many Ms as there are thousands
        if(thousands>0){
            for (int i = 0; i <thousands ; i++) {
                romanNum+="M";
            }
        }

        //if the input is greater than 100, add necessary roman numeral to string
        if(hundreds>0){
            if(hundreds==9){
                romanNum+="CM";
            }
            else if(hundreds>=5){
                romanNum+="D";
                for (int i = 0; i < hundreds-5; i++) {
                    romanNum+="C";
                }
            }
            else if(hundreds==4){
                romanNum+="CD";
            }
            else{
                for (int i = 0; i < hundreds; i++) {
                    romanNum+="C";
                }
            }
        }
        //if the number has a tens digit,add appropriate roman numeral to string
        if(tens>0){
            if(tens==9){
                romanNum+="XC";
            }
            else if(tens>=5){
                romanNum+="L";
                for (int i = 0; i < tens-5; i++) {
                    romanNum+="X";
                }
            }
            else if(tens==4){
                romanNum+="XL";
            }
            else{
                for (int i = 0; i < tens; i++) {
                    romanNum+="X";
                }
            }
        }
        //if the input has a ones digit, add appropriate roman numerals to string
        if(ones>0){
            if(ones==9){
                romanNum+="IX";
            }
            else if(ones>=5){
                romanNum+="V";
                for (int i = 0; i < ones-5; i++) {
                    romanNum+="I";
                }
            }
            else if(ones==4){
                romanNum+="IV";
            }
            else{
                for (int i = 0; i < ones; i++) {
                    romanNum+="I";
                }
            }
        }
        System.out.println("Your number in Roman numerals is " + romanNum);

    }
}
/*
Roman numbers. Write a program that converts a positive integer into the Roman number system. The Roman number system has digits
							I 1
						    V 5
							X 10
							L 50
							C 100
							D 500
							M 1,000
			Numbers are formed according to the following rules:
			a.Only numbers up to 3,999 are represented.
			b.As in the decimal system, the thousands, hundreds, tens, and ones are expressed separately.
			c.The numbers 1 to 9 are expressed asAs you can see, an I preceding a V or X is subtracted from the value, and you can never have more than three I’s in a row.
			d.Tens and hundreds are done the same way, except that the letters X, L, C and C, D, M are used instead of I, V, X, respectively.
			Your program should take an input, such as 1978, and convert it to Roman numerals, MCMLXXVIII.
 */