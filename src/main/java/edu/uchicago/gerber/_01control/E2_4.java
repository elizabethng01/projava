package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E2_4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter two integers:");
        int int1 = in.nextInt();
        int int2 = in.nextInt();
        int sum = int1+int2;
        int difference = int1-int2;
        int product = int1*int2;
        double average = (int1+int2)/2.0;
        int distance = Math.abs(difference);
        int larger = Math.max(int1,int2);
        int smaller = Math.min(int1,int2);
        System.out.println("The sum of the numbers is: " + sum);
        System.out.println("The difference of the numbers is: " + difference);
        System.out.println("The product of the numbers is: " + product);
        System.out.println("The average of the numbers is: " + average);
        System.out.println("The distance of the numbers is: " + distance);
        System.out.println("The maximum of the numbers is: " + larger);
        System.out.println("The minimum of the numbers is: " + smaller);
        /*he sum
			•The difference
			•The product
			•The average
			•The distance (absolute value of the difference)
			•The maximum (the larger of the two)
			•The minimum (the smaller of the two)
        Hint: The max and min methods are declared in the Math class.
*/
    }

}
