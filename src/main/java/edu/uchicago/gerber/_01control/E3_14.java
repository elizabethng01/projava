package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class E3_14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a month (1-12) followed by a day:");
        int month = in.nextInt();
        int day = in.nextInt();
        //check if a valid month is entered and prompt user again if not
        if(month<1 || month>12){
            System.out.println("Please enter a valid month (1-12):");
            month = in.nextInt();
        }
        //check if a valid day has been entered and prompt user again if not
        if(day<1 || day>31){
            System.out.println("Please enter a valid day:");
            day = in.nextInt();
        }

        String season = "";
        if(month<=3){
            season = "Winter";
        }
        else if(month<=6){
            season = "Spring";
        }
        else if(month<=9){
            season = "Summer";
        }
        else {
            season = "Fall";
        }
        if (month%3==0 && day>=21){
            if(season.equals("Winter")){
                season = "Spring";
            }
            else if(season.equals("Spring")){
                season = "Summer";
            }
            else if(season.equals("Summer")){
                season = "Fall";
            }
            else{
                season = "Winter";
            }
        }
        System.out.println("The season is " + season);
    }
}
/*
The following algorithm yields the season (Spring, Summer, Fall, or Winter) for a given month and day.
			If month is 1, 2, or 3, season = "Winter"
			Else if month is 4, 5, or 6, season = "Spring"
			Else if month is 7, 8, or 9, season = "Summer"
			Else if month is 10, 11, or 12, season = "Fall"
			If month is divisible by 3 and day >= 21
			   If season is "Winter", season = "Spring"
			   Else if season is "Spring", season = "Summer"
			   Else if season is "Summer", season = "Fall"
			   Else season = "Winter"
*/