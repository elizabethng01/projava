package edu.uchicago.gerber._01control;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class P3_7 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter the income: ");
        double income = in.nextDouble();
        double tax = 0;
        //if income < 50,000, tax at 1%
        if(income<50_000){
            tax+=income*0.01;
        }
        //if income >=50,000, tax first 50,000 at 1%
        if(income>=50_000){
            tax+=50_000*0.01;
            //if income has not hit next tax bracket yet, tax 2% for anything over 50,000
            if(income<=75_000){
                tax+= (income-50_000)*0.02;
            }
        }
        //if income>75,000, tax income from 50,000-75,000 at 2%
        if(income>75_000){
            tax+=(75_000-50_000)*0.02;
            //if income has not hit next tax bracket yet, tax 3% for anything over 75,000
            if(income<=100_000){
                tax+= (income-75_000)*0.03;
            }
        }
        //if income>100,000, tax income from 75,000-100,000 at 3%
        if(income>100_000){
            tax+=(100_000-75_000)*0.03;
            //if income has not hit next tax bracket yet, tax 4% for anything over 100,000
            if(income<=250_000){
                tax+= (income-100_000)*0.04;
            }
        }
        //if income>250,000, tax income from 100,000-250,000 at 4%
        if(income>250_000){
            tax+=(250_000-100_000)*0.04;
            //if income has not hit next tax bracket yet, tax 5% for anything over 250,000
            if(income<=500_000){
                tax+= (income-250_000)*0.05;
            }
        }
        //if income>500,000, tax income from 250,000-500,000 at 5%
        //tax anything over 500,000 at 6%
        if(income>500_000){
            tax+=(500_000-250_000)*0.05;
            tax+= (income-500_000)*0.06;
        }

        System.out.println("The total income tax is " + tax);

    }
}