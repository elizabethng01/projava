package edu.uchicago.gerber._01control;
import java.util.Scanner;
public class E2_6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a measurement in meters: ");
        double meters = in.nextDouble();
        //1 mile = 1609 meters, cast to int for whole number
        int miles = (int) meters/1609;
        //1 meter = 3.28084 feet, cast to int for whole number
        int feet = (int) ((meters - (miles*1609))*3.28084);
        //1 meter = 39.37 inches, multiply by 100 and round then dividr by 100 to round to 2 decimal places
        double inches = Math.round((meters - (miles*1609) - feet/3.28084)*39.37*100)/100.0;

        System.out.println("Your measurement is: \n" + miles + " miles, " + feet + " feet, and " + inches + " inches.");
    }
}
/*Write a program that prompts the user for a measurement in meters and then converts it to miles, feet, and inches.*/