package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a number: ");
        int decNum = in.nextInt();

        //String to store binary digits
        String bin = "";

        //find remainder after dividing by 2 and replace with number / 2 until we reach 0
        while (decNum>0){
            int digit = decNum%2;
            //convert digit to string and add to binary string
            bin += String.valueOf(digit);
            decNum = decNum/2;
        }
        System.out.print("Your number in binary is ");
        //loop through binary string to print in reverse
        for (int i = bin.length()-1; i >= 0; i--) {
            System.out.print(bin.charAt(i));
        }
    }

}
/*
Write a program that reads a number and prints all of its binary digits:
Print the remainder number % 2, then replace the number with number / 2.
Keep going until the number is 0. For example, if the user provides the input 13, the output should be
			1
			0
			1
			1
 */
