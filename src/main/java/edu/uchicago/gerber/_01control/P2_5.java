package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class P2_5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input a price: ");
        float price =  in.nextFloat();
        int dollars = (int) price;
        int cents = (int) ((price-dollars)*100+0.5);
        System.out.println("Your price is " + dollars + " dollars and " + cents + " cents.");

    }
}
