package edu.uchicago.gerber._04interfaces.P9_1;

import java.time.LocalTime;

public class Clock {

    private int hours;
    private int minutes;

    public Clock(){
        this.hours = getHours();
        this.minutes = getMinutes();
    }

    public int getHours(){
        return LocalTime.now().getHour();
    }
    public int getMinutes(){
        return LocalTime.now().getMinute();
    }

    public String getTime(){
        return (getHours() + ":" + getMinutes());
    }

}
