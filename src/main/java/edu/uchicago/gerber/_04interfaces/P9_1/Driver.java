package edu.uchicago.gerber._04interfaces.P9_1;

public class Driver {
    public static void main(String[] args) {
        Clock clocky = new Clock();
        System.out.println(clocky.getHours());
        System.out.println(clocky.getTime());
        WorldClock wocky = new WorldClock(3);
        System.out.println(wocky.getHours());
        System.out.println(wocky.getTime());
    }
}
