package edu.uchicago.gerber._04interfaces.P9_1;

import java.time.LocalTime;

public class WorldClock extends Clock {

    private int offset;

    public WorldClock(int offset){
        super();
        this.offset = offset;
    }

    public int getHours(){
        return super.getHours()+offset;
    }

}
