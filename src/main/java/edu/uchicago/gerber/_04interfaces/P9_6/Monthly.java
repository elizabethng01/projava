package edu.uchicago.gerber._04interfaces.P9_6;

public class Monthly extends Appointment {

    //year and month irrelevant for monthly appointment
    public Monthly(String description, int day){
        super(description,-1,-1,day);
    }

    //true only if it is the same day of each month
    @Override
    public boolean occursOn(int year, int month, int day) {
        return this.getDay()==day;
    }

    public String toString(){
        return "Monthly appointment for: " + this.getDescription() + " occuring on day " + getDay() + " of each month";
    }
}
