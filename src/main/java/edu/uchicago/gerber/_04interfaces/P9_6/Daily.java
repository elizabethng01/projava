package edu.uchicago.gerber._04interfaces.P9_6;

public class Daily extends Appointment {

    //year and month and day do not matter for daily appointments
    public Daily(String description){
        super(description,-1,-1,-1);
    }

    //daily appointment occurs every day
    @Override
    public boolean occursOn(int year, int month, int day) {
        return true;
    }

    public String toString(){
        return "Daily appointment for: " + this.getDescription();
    }
}
