package edu.uchicago.gerber._04interfaces.P9_6;

import java.time.LocalDateTime;

public class Appointment {
    private String description;
    private int year;
    private int month;
    private int day;

    public Appointment(){
        this.description = "This is an appointment.";
        this.year = LocalDateTime.now().getYear();
        this.month = LocalDateTime.now().getMonthValue();
        this.day = LocalDateTime.now().getDayOfMonth();
    }
    public Appointment(String description, int year, int month, int day){
        this.description = description;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public boolean occursOn(int year, int month, int day){
        return (this.year==year && this.month==month && this.day==day);
    }

    public int getDay() {
        return day;
    }

    public String getDescription() {
        return description;
    }

    public String toString(){
        return "Appointment for: " + description + " on " + month + "/" + day + "/" + year;
    }
}
