package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Appointment[] appts = new Appointment[10];
        appts[0] = new Onetime("dentist",2021,12,5);
        appts[1] = new Daily("shower");
        appts[2] = new Monthly("haircut",2);
        appts[3] = new Monthly("vet",5);
        appts[4] = new Onetime("doctor",2021,11,5);
        appts[5] = new Daily("dinner");
        appts[6] = new Monthly("facial",10);
        appts[7] = new Onetime("optometrist", 2021,11,11);
        appts[8] = new Daily("therapist");
        appts[9] = new Monthly("clean car",8);

        Scanner in = new Scanner(System.in);
        System.out.println("Enter a year, month (1-12), and day (1-31) to see what appointments occur on that date:");
        int year = 0;
        int month =0;
        int day =0;
        try{
            year = in.nextInt();
            month = in.nextInt();
            day = in.nextInt();
            System.out.println("Here are the appointments that occur on " + month + "/" + day + "/" + year);
            for(Appointment appt: appts){
                if(appt.occursOn(year,month,day)){
                    System.out.println(appt);
                }
            }
        } catch (Exception e) {
            System.out.println("You did not input a valid date");
        }

    }
}
