package edu.uchicago.gerber._04interfaces.E9_13;

import java.awt.*;

public class Driver {
    public static void main(String[] args) {
        BetterRectangle tangle = new BetterRectangle(1,1,5,4);
        System.out.println(tangle.getPerimeter());
        System.out.println(tangle.getArea());
        System.out.println(tangle);
    }
}
