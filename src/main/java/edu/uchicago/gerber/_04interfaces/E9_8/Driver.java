package edu.uchicago.gerber._04interfaces.E9_8;

public class Driver {
    public static void main(String[] args) {
        BasicAccount accounty = new BasicAccount();
        System.out.println(accounty.getBalance());
        accounty.deposit(100);
        System.out.println(accounty.getBalance());
        accounty.withdraw(200);
        accounty.withdraw(18);
        System.out.println(accounty.getBalance());

    }
}