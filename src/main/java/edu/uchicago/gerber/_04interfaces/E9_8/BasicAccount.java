package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccount extends BankAccount {
    /* Implement a subclass of BankAccount from How To 9.1 called BasicAccount whose withdraw method will not withdraw
    more money than is currently in the account.
    */

    public BasicAccount(){
        super();
    }

    @Override
    public void withdraw(double amount){
        if (amount>this.getBalance()){
            System.out.println("Cannot withdraw more money than is currently in the account");
        }
        else{
            super.withdraw(amount);
        }
    }

}
