package edu.uchicago.gerber._04interfaces.E9_17;

import java.util.ArrayList;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        ArrayList<SodaCan> cans = new ArrayList<>();
        while(true){
            System.out.println("Enter the height and radius of a soda can: ");
            try{
                double height = in.nextDouble();
                double radius = in.nextDouble();
                cans.add(new SodaCan(height,radius));
                System.out.println("Would you like to add another can? (yes/no)");
            } catch (Exception e) {
                System.out.println("Your input was incorrect.");
            }
            String cont = in.next();
            if(cont.charAt(0)=='n' || cont.charAt(0)=='N'){
                break;
            }
        }

        double totalSA = 0;
        for(SodaCan can: cans){
            totalSA+=can.getMeasure();
        }
        //store average surface area to two decimal places
        double avgSA = (double) Math.round(totalSA*100/cans.size())/100;

        System.out.println("The average surface area of the cans is: " + avgSA);
    }
}
