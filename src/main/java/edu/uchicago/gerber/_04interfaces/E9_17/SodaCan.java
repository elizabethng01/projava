package edu.uchicago.gerber._04interfaces.E9_17;

public class SodaCan implements Measurable {
    private double height;
    private double radius;

    public SodaCan(double height, double radius){
        this.height=height;
        this.radius=radius;
    }

    public SodaCan(int height, int radius){
        this.height = (double) height;
        this.radius = (double) radius;
    }

    @Override
    public double getMeasure() {
        return this.getSurfaceArea();
    }

    //formula = 2*pi*r(r+h)
    public double getSurfaceArea(){
        return 2*Math.PI*radius*(height+radius);
    }

    //formula = pi*r*r*h
    public double getVolume(){
        return Math.PI*radius*radius*height;
    }
}
