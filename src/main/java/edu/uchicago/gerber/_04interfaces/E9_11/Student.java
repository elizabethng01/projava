package edu.uchicago.gerber._04interfaces.E9_11;

public class Student extends Person {

    private String major;

    public Student(){
        super();
        this.major = "Undeclared";
    }

    public Student(String name, int birthYear, String major){
        super(name,birthYear);
        this.major = major;
    }

    public String toString(){
        return (super.toString() + " " + this.getName() + " is a student whose major is " + this.major + ".");
    }
}
