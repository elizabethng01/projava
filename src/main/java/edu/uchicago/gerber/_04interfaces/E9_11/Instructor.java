package edu.uchicago.gerber._04interfaces.E9_11;

public class Instructor extends Person {

    private double salary;

    public Instructor(){
        super();
        this.salary = 0.0;
    }

    public Instructor(String name, int birthYear, double salary){
        super(name,birthYear);
        this.salary = salary;
    }

    public String toString(){
        return (super.toString() + " " + this.getName() + " is an instructor whose salary is $" + this.salary + ".");
    }
}
