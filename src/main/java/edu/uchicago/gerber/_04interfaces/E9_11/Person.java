package edu.uchicago.gerber._04interfaces.E9_11;

public class Person {

    private String name;
    private int birthYear;

    public Person(){
        this.name = "Unnamed";
        this.birthYear = 2021;
    }
    public Person(String name, int birthYear){
        this.name = name;
        this.birthYear = birthYear;
    }

    public String getName(){
        return name;
    }

    public int getYear(){
        return birthYear;
    }

    public String toString(){
        return (name + " was born in " + birthYear + ".");
    }

}
