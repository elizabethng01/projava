package edu.uchicago.gerber._04interfaces.E9_11;

public class Driver {
    public static void main(String[] args) {
        Student stu = new Student();
        Instructor bill = new Instructor();
        Student mabel = new Student("mabel", 1994,"bread making");
        Instructor toad = new Instructor("toad",1922,90210);
        System.out.println(stu);
        System.out.println(bill);
        System.out.println(mabel);
        System.out.println(toad);
    }
}
