package edu.uchicago.gerber._02arrays;
/*
E6.1 Write a program that initializes an array with ten random integers and then prints four lines of output, containing
        •Every element at an even index.
        •Every even element.
        •All elements in reverse order.
        •Only the first and last element.
*/
import java.util.Random;

public class E6_1 {
        public static void main(String[] args) {
                int arrLength = 10;
                int[] randArr = new int[arrLength];
                Random rand = new Random();
                int upperBound = 100; //to set upper bound to choose random integer from
                //initialise randArr with random numbers from 0 to upperBound
                for (int i=0; i<randArr.length;i++) {
                        randArr[i] = rand.nextInt(upperBound);
                }
                //print every element at even index
                System.out.println("Every element at an even index: ");
                for(int i = 0; i<randArr.length; i+=2){
                        System.out.print(randArr[i] + " ");
                }

                //print every even element
                System.out.println("\nEvery even element: ");
                for (int val:randArr){
                        if(val%2==0){
                                System.out.print(val + " ");
                        }
                }

                //print elements in reverse
                System.out.println("\nEvery element in reverse: ");
                for (int i=randArr.length-1; i>=0;i--){
                        System.out.print(randArr[i] + " ");
                }

                //print first and last elements
                System.out.println("\nFirst and last element: ");
                System.out.println(randArr[0] + " " + randArr[randArr.length-1]);

        }
}
