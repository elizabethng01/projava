package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

public class E6_16 {
    //method to create bar chart given array of values
    public static char[][] barChart(int[] values){
        int rows = 20; //tallest bar is 20 rows as dictated by question
        int columns = values.length;
        char[][] chart = new char[rows][columns];
        //identify maximum entry in values to be drawn with 20 asterisks
        int maxEntry = values[0];
        for (int val:values){
            if (val>maxEntry){
                maxEntry = val;
            }
        }
        for (int i = 0; i < columns; i++) {
            //make height of bar proportional to max entry
            int height = (values[i]*20)/maxEntry;
            for (int j = 0; j < rows; j++) {
                if(j<height) {
                    chart[j][i]='*';
                }
                else {
                    chart[j][i]=' ';
                }
            }
        }
        //flip rows so bar chart builds up from bottom to top
        char[][] result = new char[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result[i][j]=chart[rows-1-i][j];
            }
        }
        return result;

    }
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Please input some values:");
        //store input in a string array split by spaces
        String[] input = in.nextLine().split(" ");

        //create new integer array and parse string values as integer values
        int[] values = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            values[i] = Integer.parseInt(input[i]);
        }

        char[][] chart = barChart(values);
        for (int i = 0; i < 20 ; i++) {
            for (int j = 0; j < values.length ; j++) {
                System.out.print(chart[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}
