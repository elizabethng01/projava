package edu.uchicago.gerber._02arrays;

public class P5_8 {
    public static boolean isLeapYear(int year){
        boolean isLeap = false;
        //if the year is divisible by 4 and either not divisible by 100 or divisible by 400, it is a leap year
        if(year%4==0 && (year%100!=0 || year%400 ==0)){
            return true;
        }
        return isLeap;
    }
}
