package edu.uchicago.gerber._02arrays;

public class P5_24 {
    //method to convert roman numeral character to an integer
    public static int romanCharToInt(char chr){
        if(chr=='M'){ return 1000; }
        else if(chr=='D'){ return 500; }
        else if(chr=='C'){ return 100; }
        else if(chr=='L'){ return 50; }
        else if(chr=='X'){ return 10; }
        else if(chr=='V'){ return 5; }
        else if(chr=='I'){ return 1; }
        else {return 0;}
    }

    public static int romanToDecimal(String roman){
        int total = 0;
        //loop through characters in roman numeral string
        for (int i = 0; i < roman.length(); i++) {
            int currVal = romanCharToInt(roman.charAt(i));
            //if we are at the end of the roman numeral, add value to the total
            if(i==roman.length()-1){
                total+=currVal;
            }
            else {
                int nextVal = romanCharToInt(roman.charAt(i+1));
                //if the current roman character is greater than or equal to the next roman character, add to the total
                if(currVal>=nextVal) {
                    total += currVal;
                }
                else{
                    //if the current roman character is less than the next roman character, add the difference to the total
                    int diff = nextVal - currVal;
                    total += diff;
                    i++; //increment i again to skip over next character as it has been accounted for already
                }
            }

        }
        return total;
    }

}
