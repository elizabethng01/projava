package edu.uchicago.gerber._02arrays;
/*
Write a program that generates a sequence of 20 random values between 0 and 99 in an array,
prints the sequence, sorts it, and prints the sorted sequence. Use the sort method from the standard Java library.
 */

import java.util.Arrays;
import java.util.Random;

public class E6_12 {
    public static void main(String[] args) {
        int arrLength = 20;
        int[] randArr = new int[arrLength];
        Random rand = new Random();
        //upper bound for random integers
        int upperBound = 100;
        //set array values to random integer from 0 to upper bound
        for (int i = 0; i < randArr.length; i++) {
            randArr[i]=rand.nextInt(upperBound);
        }

        System.out.println("Random array: ");
        for (int val: randArr){
            System.out.print(val + " ");
        }
        //sort array
        Arrays.sort(randArr);

        System.out.println("\nSorted array: ");
        for (int val: randArr){
            System.out.print(val + " ");
        }
    }
}
