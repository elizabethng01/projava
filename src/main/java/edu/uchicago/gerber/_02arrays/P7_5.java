package edu.uchicago.gerber._02arrays;
/*
The CSV (or comma-separated values) format is commonly used for tabular data. Each table row is a line, with columns separated by commas.
Items may be enclosed in quotation marks, and they must be if they contain commas or quotation marks.
Quotation marks inside quoted fields are doubled. Here is a line with four fields:

			1729, San Francisco, "Hello, World", "He asked: ""Quo vadis?"""

Write a CSVReader program that reads a CSV file, and provide methods
			int numberOfRows()
			int numberOfFields(int row)
			String field(int row, int column)
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class P7_5 {
    //static array list to store csv contents for program
    public static ArrayList<String> csvFile = new ArrayList<String>() ;

    public static int numberOfRows(){
        return csvFile.size();
    }

    public static int numberOfFields(int row) {
        String rowInput = csvFile.get(row);

        int fieldCount= 0;
        //if row is not empty, there is at least one field
        if (!rowInput.equals("")) {
            fieldCount = 1;
            //switch to track if currently looping within a quote
            boolean quote = false;

            //loop through row, if the character is ',' and not within a quote, add another field
            for (int i = 0; i < rowInput.length(); i++) {
                char currChar = rowInput.charAt(i);
                if ((currChar == ',')&&(!quote)) {
                    fieldCount+=1;
                }
                //if the character is ", flip the quote switch
                else if (currChar == '"') {
                    quote = !quote;
                }
            }
        }
        return fieldCount;
    }

    //row and column use standard indexing starting at 0
    public static String field(int row, int column){

        if(row>=numberOfRows()){
            return "Row out of bounds";
        }
        else if(column>=numberOfFields(row)){
            return "Column out of bounds";
        }

        else{
            String rowInput = csvFile.get(row);
            int commaCount = 0; //count number of commas to indicate which field we are looping through
            int startIndex = 0; //store start index of column, initialise to start of row
            int endIndex = rowInput.length(); //store end index of column, initialise to end of row
            boolean quote = false; //switch to check if looping inside a quote

            for (int i = 0; i<rowInput.length();i++){
                char currVal = rowInput.charAt(i);
                //if the current character is a comma and not in a quote, increment comma count
                if ((currVal==',') && (!quote)){
                    commaCount+=1;
                    //if we are at the comma before the desired column, update starting index
                    if(commaCount==column){
                        startIndex = i+2; //to start after comma and space
                    }
                    //if we are at the comma at the end of the desired column, update ending index
                    else if(commaCount==column+1){
                        endIndex = i;
                        break;
                    }
                }
                else {
                    //if current character is ", flip quote switch
                    if (currVal=='"'){
                        quote=!quote;
                    }
                }
            }
            //return substring containing desired column
            return rowInput.substring(startIndex,endIndex);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("What is the name of the input file?");
        String inName = in.next();
        try{
            File inputFile = new File(inName);
            Scanner read = new Scanner(inputFile);
            while(read.hasNextLine()){
                csvFile.add(read.nextLine());
            }
            read.close();
        } catch (FileNotFoundException e) {
            System.out.println("The file does not exist");
            e.printStackTrace();
        }
    }

}
