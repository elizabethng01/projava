package edu.uchicago.gerber._02arrays;
/*
E7.4Write a program that reads a file containing text. Read each line and send it to the output file, preceded by line numbers. If the input file is
			Mary had a little lamb
			Whose fleece was white as snow.
			And everywhere that Mary went,
			The lamb was sure to go!
then the program produces the output file
			1 Mary had a little lamb
            2 Whose fleece was white as snow.
            3 And everywhere that Mary went,
            4 The lamb was sure to go!
        The line numbers are enclosed in /* */ /* delimiters so that the program can be used for numbering Java source files.
        Prompt the user for the input and output file names.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class E7_4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("What is the name of the input file?");
        String inName = in.next();
        System.out.println("What is the name of the output file?");
        String outName = in.next();

        try{
            File inputFile = new File(inName);
            File outputFile = new File(outName);
            Scanner read = new Scanner(inputFile);
            PrintWriter write = new PrintWriter(outputFile);
            //int line count to keep track of which line number
            int lineCount = 1;
            //write each line from input file to output file with /* i */ at the front where i is the line count number
            while(read.hasNextLine()){
                write.println("/* "+ lineCount +" */ "+read.nextLine());
                lineCount++;
            }
            read.close();
            write.close();
        } catch (FileNotFoundException e) { //print error message if can't open file
            System.out.println("The file does not exist");
            e.printStackTrace();
        }
    }
}
