package edu.uchicago.gerber._02arrays;
/*
Write a method
			public static boolean equals(int[] a, int[] b)
that checks whether two arrays have the same elements in the same order.
 */
public class E6_9 {
    public static boolean equals(int[] a, int[] b){
        boolean isEqual = true;
        //if a and b are different lengths, they are not equal
        if (a.length!=b.length){
            isEqual = false;
        }
        else {
            for (int i = 0; i < a.length; i++) {
                //if any element is not the same at the same index, they are not equal
                if(a[i]!=b[i]){
                    isEqual = false;
                    break;
                }
            }
        }
        return isEqual;
    }
}
