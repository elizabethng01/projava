***** ASTEROIDS *****

The source code provided extends the base code for Asteroids provided.

MODIFICATIONS MADE:
- A debris class has been implemented that contains the code to create debris for each explosion
- A ShieldFloater has been implemented that provides the falcon with a temporary shield
    - shield are not activated immediately upon collision with the shield floater and only one shield is available at
      any time for the falcon which must be activated by pressing 'A'
- A MegaMissileFlaoter has been implemented that provides the falcon with mega bullets temporarily
    - bullets are available upon collision with the floater and cannot be collected
- An ExplodeFloater has been implemented that can be collected by the falcon
    - once 10 ExplodeFloaters are collected, the falcon can set off a space wise explosion and blow up any foe that is
      sharing the falcon's space. Mega bullets and a shield are provided immediately after the explosion is set off by
      pressing 'F'
- An EvilFalcon with accompanying EvilBullets will appear at every 3rd level
    - the evil falcon must be hit a certain number of times before it will die, this number and the evil falcon's size
      increasing each time it appears. The evil bullets will directly track the falcon and become more accurate as the
      level increases
- The falcon is now capable of hyper jumping by pressing the down arrow
    - hyper jumping takes a large amount of energy from the falcon and thus it cannot be utilized at all times, the
      falcon must wait until hyper jumping capabilities are restored before it can hyper jump again
