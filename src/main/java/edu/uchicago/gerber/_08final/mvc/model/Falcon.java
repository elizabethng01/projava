package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;
//import javafx.util.Pair;
import lombok.Data;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Falcon extends Sprite {

	// ==============================================================
	// FIELDS 
	// ==============================================================
	
	private final double THRUST = .65;

	final int DEGREE_STEP = 7;
	
	//private boolean shield = false;
	private boolean thrusting = false;
	private boolean turningRight = false;
	private boolean turningLeft = false;
	private int shield = 0;
	private int missile = 0;
	private boolean shieldOn = false;

	// ==============================================================
	// CONSTRUCTOR 
	// ==============================================================
	
	public Falcon() {
		super();
		setTeam(Team.FRIEND);


		setColor(Color.white);
		
		//put falcon in the middle.
		setCenter(new Point(Game.DIM.width / 2, Game.DIM.height / 2));
		
		//with random orientation
		setOrientation(Game.R.nextInt(360));
		
		//this is the size (radius) of the falcon
		setRadius(35);

		//Falcon uses fade.
		setFade(0);

		//be sure to set cartesian points last.
		List<Point> pntCs = new ArrayList<>();
		// Robert Alef's awesome falcon design
		pntCs.add(new Point(0,9));
		pntCs.add(new Point(-1, 6));
		pntCs.add(new Point(-1,3));
		pntCs.add(new Point(-4, 1));
		pntCs.add(new Point(4,1));
		pntCs.add(new Point(-4,1));

		pntCs.add(new Point(-4, -2));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-1, -9));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-4, -2));

		pntCs.add(new Point(-10, -8));
		pntCs.add(new Point(-5, -9));
		pntCs.add(new Point(-7, -11));
		pntCs.add(new Point(-4, -11));
		pntCs.add(new Point(-2, -9));
		pntCs.add(new Point(-2, -10));
		pntCs.add(new Point(-1, -10));
		pntCs.add(new Point(-1, -9));
		pntCs.add(new Point(1, -9));
		pntCs.add(new Point(1, -10));
		pntCs.add(new Point(2, -10));
		pntCs.add(new Point(2, -9));
		pntCs.add(new Point(4, -11));
		pntCs.add(new Point(7, -11));
		pntCs.add(new Point(5, -9));
		pntCs.add(new Point(10, -8));
		pntCs.add(new Point(4, -2));

		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(1, -9));
		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(4,-2));

		pntCs.add(new Point(4, 1));
		pntCs.add(new Point(1, 3));
		pntCs.add(new Point(1,6));
		pntCs.add(new Point(0,9));

		setCartesians(pntCs);
	}

	@Override
	public boolean isProtected() {
		return (getFade() != 255 || (shield > 0 && shieldOn));
	}

	public void setShield(int shieldTime){
		shield = shieldTime;
	}

	public void shieldSwitch(){
		shieldOn = !shieldOn;
	}

	public int getShield(){
		return shield;
	}

	public int getMissile(){
		return missile;
	}

	public void setMissile(int missileTime){
		missile = missileTime;
	}


	// ==============================================================
	// METHODS
	// ==============================================================
	@Override
	public void move() {

		super.move();

		if (isProtected()) {
			if (getFade()<255){
				setFade(getFade()+3);
			}
			else if (shieldOn){
				if(shield>0){
					shield=shield-1;
				}
				else if(shield==0){
					shieldSwitch();
				}
			}
		}


		if(missile > 0){
			missile = missile - 1;
		}


		//apply some thrust vectors using trig.
		if (thrusting) {
			double adjustX = Math.cos(Math.toRadians(getOrientation()))
					* THRUST;
			double adjustY = Math.sin(Math.toRadians(getOrientation()))
					* THRUST;
			setDeltaX(getDeltaX() + adjustX);
			setDeltaY(getDeltaY() + adjustY);
		}
		//rotate left
		if (turningLeft) {
			if (getOrientation() <= 0) {
				setOrientation(360);
			}
			setOrientation(getOrientation() - DEGREE_STEP);
		}
		//rotate right
		if (turningRight) {
			if (getOrientation() >= 360) {
				setOrientation(0);
			}
			setOrientation(getOrientation() + DEGREE_STEP);
		}

	} //end move


	//methods for moving the falcon
	public void rotateLeft() {
		turningLeft = true;
	}

	public void rotateRight() {
		turningRight = true;
	}

	public void stopRotating() {
		turningRight = false;
		turningLeft = false;
	}

	public void thrustOn() {
		thrusting = true;
	}

	public void thrustOff() {
		thrusting = false;
	}


	private int adjustColor(int colorNum, int adjust) {
		return Math.max(colorNum - adjust, 0);
	}

	@Override
	public void draw(Graphics g) {

		Color colShip;

		if(shield > 0 && shieldOn){
			colShip = Color.white;
			g.setColor(Color.green);
			g.drawOval(getCenter().x-50,getCenter().y-50,100,100);
			if(shield>20){
				g.drawOval(getCenter().x-60,getCenter().y-60,120,120);
			}
			if(shield>40){
				g.drawOval(getCenter().x-70,getCenter().y-70,140,140);
			}
			if(shield>60){
				g.drawOval(getCenter().x-80,getCenter().y-80,160,160);
			}
			if(shield>80){
				g.drawOval(getCenter().x-90,getCenter().y-90,180,180);
			}


		}
		else if (getFade() == 255) {
			colShip = Color.white;
		}
		else {
			colShip = new Color(adjustColor(getFade(), 200), adjustColor(
					getFade(), 175), getFade());
		}



		//most Sprites do not have flames, but Falcon does
		 double[] flames = { 23 * Math.PI / 24 + Math.PI / 2, Math.PI + Math.PI / 2, 25 * Math.PI / 24 + Math.PI / 2 };
		 Point[] pntFlames = new Point[flames.length];

		//thrusting
		if (thrusting) {
			g.setColor(colShip);
			if(missile > 0){
				g.setColor(Color.red);
			}
			//the flame
			for (int nC = 0; nC < flames.length; nC++) {
				if (nC % 2 != 0) //odd
				{
					//adjust the position so that the flame is off-center
					pntFlames[nC] = new Point((int) (getCenter().x + 2
							* getRadius()
							* Math.sin(Math.toRadians(getOrientation())
									+ flames[nC])), (int) (getCenter().y - 2
							* getRadius()
							* Math.cos(Math.toRadians(getOrientation())
									+ flames[nC])));

				} else //even
				{
					pntFlames[nC] = new Point((int) (getCenter().x + getRadius()
							* 1.1
							* Math.sin(Math.toRadians(getOrientation())
									+ flames[nC])),
							(int) (getCenter().y - getRadius()
									* 1.1
									* Math.cos(Math.toRadians(getOrientation())
											+ flames[nC])));

				} //end even/odd else
			} //end for loop

			g.fillPolygon(
					Arrays.stream(pntFlames)
							.map(pnt -> pnt.x)
							.mapToInt(Integer::intValue)
							.toArray(),

					Arrays.stream(pntFlames)
							.map(pnt -> pnt.y)
							.mapToInt(Integer::intValue)
							.toArray(),

					flames.length);

		} //end if flame

		draw(g,colShip);

	} //end draw()

} //end class
