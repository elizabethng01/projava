package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class Debris extends Sprite {

    //radius of a largest debris piece
    private final int LARGE_DEBRIS_RADIUS = 5;

    public Debris(Point center){
        super();

        setTeam(Team.DEBRIS);
        setColor(Color.white);
        setRadius(Game.R.nextInt(LARGE_DEBRIS_RADIUS));
        setCenter(center);


        setExpiry(10);

        //random delta-x
        setDeltaX(somePosNegValue(10));

        //random delta-y
        setDeltaY(somePosNegValue(10));

        assignRandomShape();
    }

    public void move(){
        super.move();
        setOrientation(getOrientation() + getSpin());
        expire();
    }

    public void assignRandomShape (){

        //6.283 is the max radians
        final int MAX_RADIANS_X1000 =6283;

        int sides = Game.R.nextInt( 7 ) + 17;
        List<Pair<Double, Double>> polarPairs = new ArrayList<>();
        for ( int nC = 0; nC < sides; nC++ ){
            double theta = Game.R.nextInt(MAX_RADIANS_X1000) / 1000.0;
            double r = (800 + Game.R.nextInt(200)) / 1000.0;
            polarPairs.add(new Pair<>(theta,r));
        }

        setCartesians(polarToCartesian(

                polarPairs.stream()
                        .sorted(new Comparator<Pair<Double, Double>>() {
                            @Override
                            public int compare(Pair<Double, Double> p1, Pair<Double, Double> p2) {
                                return  p1.getKey().compareTo(p2.getKey());
                            }
                        })
                        .collect(Collectors.toList()))
        );

    }

}
