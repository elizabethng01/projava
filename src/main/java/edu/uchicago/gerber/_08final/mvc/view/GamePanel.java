package edu.uchicago.gerber._08final.mvc.view;

import edu.uchicago.gerber._08final.mvc.controller.Game;
import edu.uchicago.gerber._08final.mvc.model.CommandCenter;
import edu.uchicago.gerber._08final.mvc.model.Falcon;
import edu.uchicago.gerber._08final.mvc.model.Movable;

import java.awt.*;
import java.util.Arrays;
import java.util.List;


public class GamePanel extends Panel {
	
	// ==============================================================
	// FIELDS 
	// ============================================================== 
	 
	// The following "off" vars are used for the off-screen double-bufferred image. 
	private Dimension dimOff;
	private Image imgOff;
	private Graphics grpOff;
	
	private GameFrame gmf;
	private Font fnt = new Font("SansSerif", Font.BOLD, 12);
	private Font fntBig = new Font("SansSerif", Font.BOLD + Font.ITALIC, 36);
	private FontMetrics fmt; 
	private int fontWidth;
	private int fontHeight;
	private String strDisplay = "";
	

	// ==============================================================
	// CONSTRUCTOR 
	// ==============================================================
	
	public GamePanel(Dimension dim){
	    gmf = new GameFrame();
		gmf.getContentPane().add(this);
		gmf.pack();
		initView();
		
		gmf.setSize(dim);
		gmf.setTitle("Game Base");
		gmf.setResizable(false);
		gmf.setVisible(true);
		this.setFocusable(true);
	}
	
	
	// ==============================================================
	// METHODS 
	// ==============================================================
	
	private void drawScore(Graphics g) {
		g.setColor(Color.white);
		g.setFont(fnt);
		if (CommandCenter.getInstance().getScore() != 0) {
			g.drawString("SCORE :  " + CommandCenter.getInstance().getScore(), fontWidth, fontHeight);
		} else {
			g.drawString("SCORE : 0", fontWidth, fontHeight);
		}
	}

	private void drawLevel(Graphics g) {
		g.setColor(Color.white);
		g.setFont(fnt);
		if (CommandCenter.getInstance().getScore()==0){
			g.drawString("LEVEL : 1", fontWidth, fontHeight+20);
		} else {
			g.drawString("LEVEL : " + CommandCenter.getInstance().getLevel(), fontWidth, fontHeight + 20);
		}
	}

	public void update(Graphics g) {
		if (grpOff == null || Game.DIM.width != dimOff.width
				|| Game.DIM.height != dimOff.height) {
			dimOff = Game.DIM;
			imgOff = createImage(Game.DIM.width, Game.DIM.height);
			grpOff = imgOff.getGraphics();
		}
		// Fill in background with black.
		grpOff.setColor(Color.black);
		grpOff.fillRect(0, 0, Game.DIM.width, Game.DIM.height);

		drawScore(grpOff);
		drawLevel(grpOff);
		
		if (!CommandCenter.getInstance().isPlaying()) {
			displayTextOnScreen();
		} else if (CommandCenter.getInstance().isPaused()) {
			strDisplay = "Game Paused";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);
		}
		
		//playing and not paused!
		else {

			iterateMovables(grpOff,
			CommandCenter.getInstance().getMovDebris(),
			CommandCenter.getInstance().getMovFloaters(),
			CommandCenter.getInstance().getMovFoes(),
			CommandCenter.getInstance().getMovFriends());


			drawNumberShipsLeft(grpOff);
			drawExplosionMeter(grpOff);
			drawHyperMeter(grpOff);
			if(CommandCenter.getInstance().getFalcon().getMissile()>0){
				drawMissileMeter(grpOff,CommandCenter.getInstance().getFalcon().getMissile());
			}

			if(CommandCenter.getInstance().getFalcon().getShield()>0){
				drawShieldMeter(grpOff,CommandCenter.getInstance().getFalcon().getShield());
			}

			//if((CommandCenter.getInstance().getLevel())%5==0 &&
			if(CommandCenter.getInstance().getEvilFalcon()!=null) {
				if(CommandCenter.getInstance().getEvilFalcon().getHitsMade()==CommandCenter.getInstance().getEvilFalcon().getHitsNeeded()){
					undrawEvilFalconMeter(grpOff);
				}
				else{
					drawEvilFalconMeter(grpOff,CommandCenter.getInstance().getEvilFalcon().getHealth());
				}

			}

			if (CommandCenter.getInstance().isGameOver()) {
				CommandCenter.getInstance().setPlaying(false);
			}
		}

		//draw the double-Buffered Image to the graphics context of the panel
		g.drawImage(imgOff, 0, 0, this);
	} 


	
	//for each movable array, process it.
	@SafeVarargs
	private final void iterateMovables(Graphics g, List<Movable>... arrayOfListMovables){
		
		for (List<Movable> movMovs : arrayOfListMovables) {
			for (Movable mov : movMovs) {

				mov.move();
				mov.draw(g);


			}
		}
		
	}


	private void drawNumberShipsLeft(Graphics g){
		int numFalcons = CommandCenter.getInstance().getNumFalcons();
		while (numFalcons > 0){
			drawOneShipLeft(g, numFalcons--);
		}
	}

	// Draw the number of falcons left on the bottom-right of the screen. Upside-down, but ok.
	private void drawOneShipLeft(Graphics g, int offSet) {
		Falcon falcon = CommandCenter.getInstance().getFalcon();

		g.setColor(Color.white);

		g.drawPolygon(
					Arrays.stream(falcon.getCartesians())
							.map(pnt -> pnt.x + Game.DIM.width - 5 - (25 * offSet))
							.mapToInt(Integer::intValue)
							.toArray(),

					Arrays.stream(falcon.getCartesians())
							.map(pnt -> pnt.y + Game.DIM.height - 55)
							.mapToInt(Integer::intValue)
							.toArray(),

					falcon.getCartesians().length);

	}

	private void drawExplosionMeter(Graphics g){
		int explodecount = CommandCenter.getInstance().getExplode();
		g.setColor(Color.white);
		g.drawRect(48,200,18,204);
		g.setColor(Color.yellow);
		g.fillRect(50,402-explodecount*20,15,explodecount*20);
	}
	private void drawHyperMeter(Graphics g){
		int hyperCount = CommandCenter.getInstance().getHyper();
		g.setColor(Color.white);
		g.drawRect(48,450,18,204);
		//g.setColor(Color.white);
		g.fillRect(50,652-hyperCount*2,15,hyperCount*2);
	}

	private void drawEvilFalconMeter(Graphics g, int health){
		g.setColor(Color.white);
		g.drawRect(Game.DIM.width-50,315,18,204);
		g.setColor(Color.magenta);
		g.fillRect(Game.DIM.width-48,317+(health*2),15,(200-health*2));
	}

	private void undrawEvilFalconMeter(Graphics g){
		g.setColor(Color.black);
		g.drawRect(Game.DIM.width-50,315,18,204);
	}

	private void drawShieldMeter(Graphics g, int shield){
		g.setColor(Color.white);
		g.drawRect(60,Game.DIM.height-75,204,14);
		g.setColor(Color.green);
		g.fillRect(62,Game.DIM.height-73,shield*2,10);
	}
	private void drawMissileMeter(Graphics g, int missile){
		g.setColor(Color.white);
		g.drawRect(300,Game.DIM.height-75,204,14);
		g.setColor(Color.red);
		g.fillRect(302,Game.DIM.height-73,missile*2,10);
	}

	private void drawEFalconMeter(Graphics g, int health){
		g.setColor(Color.white);
		g.drawRect(540,Game.DIM.height-75,204,14);
		g.setColor(Color.magenta);
		g.fillRect(542,Game.DIM.height-73,200-(health*2),10);
	}

	private void undrawEFalconMeter(Graphics g){
		g.setColor(Color.black);
		g.drawRect(Game.DIM.width-50,200,18,204);
	}


	private void initView() {
		Graphics g = getGraphics();			// get the graphics context for the panel
		g.setFont(fnt);						// take care of some simple font stuff
		fmt = g.getFontMetrics();
		fontWidth = fmt.getMaxAdvance();
		fontHeight = fmt.getHeight();
		g.setFont(fntBig);					// set font info
	}
	
	// This method draws some text to the middle of the screen before/after a game
	private void displayTextOnScreen() {

		strDisplay = "GAME OVER";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4 - 20);

		strDisplay = "Use the arrow keys to turn and thrust";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 20);

		strDisplay = "Use the down key for hyper space";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 60);

		strDisplay = "Use the space bar to fire";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 100);

		strDisplay = "'S' to Start";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 140);

		strDisplay = "'P' to Pause";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 180);

		strDisplay = "'Q' to Quit";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 220);


		strDisplay = "'F' to Explode";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 260);

		strDisplay = "'A' to Shield";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 300);

		strDisplay = "Collect BLUE floaters for an extra life";
		grpOff.setColor(Color.BLUE);
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 340);

		strDisplay = "Collect GREEN floaters for a shield";
		grpOff.setColor(Color.GREEN);
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 380);

		strDisplay = "Collect RED floaters for mega bullets";
		grpOff.setColor(Color.RED);
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 420);

		strDisplay = "Collect 10 YELLOW floaters to set off a space wide explosion";
		grpOff.setColor(Color.YELLOW);
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ fontHeight + 460);
	}
	

}