package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EvilFalcon extends Sprite {

	// ==============================================================
	// FIELDS
	// ==============================================================
	private int hitsNeeded = 0;
	private int hitsMade = 0;
	private final int TOTALJUMP = 100;
	private int jump;
	// ==============================================================
	// CONSTRUCTOR
	// ==============================================================

	public EvilFalcon() {
		super();

		setTeam(Team.FOE);
		jump = TOTALJUMP;
		setColor(Color.magenta);


		//with random orientation
		setOrientation(Game.R.nextInt(360));
		//the spin will be either plus or minus 0-9

		//setSpin(somePosNegValue(10));

		//random delta-x
		setDeltaX(somePosNegValue(10));

		//random delta-y
		setDeltaY(somePosNegValue(10));

		//this is the size (radius) of the falcon
		setRadius(35);

		//be sure to set cartesian points last.
		List<Point> pntCs = new ArrayList<>();
		// Robert Alef's awesome falcon design
		pntCs.add(new Point(0, 6));
		pntCs.add(new Point(-1, 6));
		pntCs.add(new Point(-1, 3));
		pntCs.add(new Point(-4, 1));
		pntCs.add(new Point(4, 1));
		pntCs.add(new Point(-4, 1));

		pntCs.add(new Point(-4, -2));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-1, -9));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-4, -2));

		pntCs.add(new Point(-15, -8));
		pntCs.add(new Point(-5, -9));
		pntCs.add(new Point(-7, -11));
		pntCs.add(new Point(-4, -11));
		pntCs.add(new Point(-2, -9));
		pntCs.add(new Point(-2, -10));
		pntCs.add(new Point(-1, -10));
		pntCs.add(new Point(-1, -9));
		pntCs.add(new Point(1, -9));
		pntCs.add(new Point(1, -10));
		pntCs.add(new Point(2, -10));
		pntCs.add(new Point(2, -9));
		pntCs.add(new Point(4, -11));
		pntCs.add(new Point(7, -11));
		pntCs.add(new Point(5, -9));
		pntCs.add(new Point(15, -8));
		pntCs.add(new Point(4, -2));

		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(1, -9));
		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(4, -2));

		pntCs.add(new Point(4, 1));
		pntCs.add(new Point(1, 3));
		pntCs.add(new Point(1, 6));
		pntCs.add(new Point(0, 6));

		setCartesians(pntCs);
	}

	public EvilFalcon(int radius, int hitsNeeded){
		new EvilFalcon();
		setRadius(radius);
		this.hitsNeeded = hitsNeeded;
	}


	// ==============================================================
	// METHODS
	// ==============================================================
	public void move() {
		super.move();

		//update orientation of evil falcon so it is always pointing towards the falcon,
		double angle = Math.atan2(getCenter().y-CommandCenter.getInstance().getFalcon().getCenter().y,
				getCenter().x-CommandCenter.getInstance().getFalcon().getCenter().x) + Math.PI;

		//if falcon moved past 180 degrees from falcon then just jump
		if(Math.abs(getOrientation() - (Math.toDegrees(angle))) > 180){
			jump = 0;
		}
		setOrientation((int) (Math.toDegrees(angle)));
		if(jump>0){
			jump = jump-1;
		}
		//jumps place to place to make it more difficult to attack, set random new center point
		else if (jump==0){
			jump = TOTALJUMP;
			setCenter(new Point(Game.R.nextInt(Game.DIM.width-100)+50,Game.R.nextInt(Game.DIM.height-100)+50));
		}
		setOrientation(getOrientation()+getSpin());
	}

	public int getHitsMade(){
		return hitsMade;
	}
	public void incHitsMade(){
		hitsMade+=1;
	}
	public int getHitsNeeded(){
		return hitsNeeded;
	}
	public void setHitsNeeded(int hitsNeeded){
		this.hitsNeeded = hitsNeeded;
	}

	public int getHealth(){
		double healthPercentage = ((double) hitsMade/hitsNeeded)*100;
		return (int) Math.round(healthPercentage);
	}
}