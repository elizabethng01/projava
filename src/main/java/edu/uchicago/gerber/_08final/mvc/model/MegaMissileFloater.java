package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

//for big bullets!

public class MegaMissileFloater extends Sprite{
    public MegaMissileFloater(){
        super();
        setTeam(Team.FLOATER);
        setExpiry(200);
        setRadius(45);
        setColor(Color.red);

        //set random DeltaX
        setDeltaX(somePosNegValue(10));

        //set rnadom DeltaY
        setDeltaY(somePosNegValue(10));

        //set random spin
        setSpin(somePosNegValue(10));

        //random point on the screen
        setCenter(new Point(Game.R.nextInt(Game.DIM.width),
                Game.R.nextInt(Game.DIM.height)));

        //random orientation
        setOrientation(Game.R.nextInt(360));

        //always set cartesian points last
        List<Point> pntCs = new ArrayList<Point>();
        pntCs.add(new Point(2, 0));
        pntCs.add(new Point(1,2));
        pntCs.add(new Point(2, 2));
        pntCs.add(new Point(0, 6));

        pntCs.add(new Point(-2, 2));
        pntCs.add(new Point(-1,2));
        pntCs.add(new Point(-2, 0));

        pntCs.add(new Point(-1, -2));
        pntCs.add(new Point(-2, -2));
        pntCs.add(new Point(0, -6));

        pntCs.add(new Point(2, -2));
        pntCs.add(new Point(1, -2));

        setCartesians(pntCs);

    }

    public void move() {
        super.move();
        //a newShipFloater spins
        setOrientation(getOrientation() + getSpin());
        //and it also expires
        expire();

    }
}
