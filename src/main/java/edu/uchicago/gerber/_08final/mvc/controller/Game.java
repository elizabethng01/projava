package edu.uchicago.gerber._08final.mvc.controller;

import edu.uchicago.gerber._08final.mvc.model.*;
import edu.uchicago.gerber._08final.mvc.view.GamePanel;


import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Random;
// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

	// ===============================================
	// FIELDS
	// ===============================================

	public static final Dimension DIM = new Dimension(1100, 830); //the dimension of the game.
	private GamePanel gmpPanel;
	//this is used throughout many classes.
	public static Random R = new Random();

	public final static int ANI_DELAY = 50; // milliseconds between screen
											// updates (animation)

	public final static int FRAMES_PER_SECOND = 1000 / ANI_DELAY;

	private Thread animationThread;
	private int level = 1;
	private boolean muted = true;

	private final int PAUSE = 80, // p key
			QUIT = 81, // q key
			LEFT = 37, // rotate left; left arrow
			RIGHT = 39, // rotate right; right arrow
			UP = 38, // thrust; up arrow
			START = 83, // s key
			FIRE = 32, // space key
			MUTE = 77, // m-key mute

	// for possible future use
	 HYPER = 40, 					// down key
	 SHIELD = 65, 				// a key arrow
	// NUM_ENTER = 10, 				// hyp
	 EXPLODE = 70; 					// fire special weapon;  F key

	private Clip clpThrust;
	private Clip clpMusicBackground;

	//spawns at different times
	private static final int SPAWN_NEW_SHIP_FLOATER = FRAMES_PER_SECOND * 30;
	private static final int SPAWN_NEW_SHIELD_FLOATER = FRAMES_PER_SECOND * 29;
	private static final int SPAWN_NEW_MISSILE_FLOATER = FRAMES_PER_SECOND * 31;
	private static final int SPAWN_NEW_EXPLODE_FLOATER = FRAMES_PER_SECOND * 8;


	// ===============================================
	// ==CONSTRUCTOR
	// ===============================================

	public Game() {

		gmpPanel = new GamePanel(DIM);
		gmpPanel.addKeyListener(this);
		clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
		clpMusicBackground = Sound.clipForLoopFactory("music-background.wav");
	

	}

	// ===============================================
	// ==METHODS
	// ===============================================

	public static void main(String args[]) {
		//typical Swing application main method
		EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
					public void run() {
						try {
							Game game = new Game(); // construct itself
							game.fireUpAnimThread();

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	private void fireUpAnimThread() { // called initially
		if (animationThread == null) {
			animationThread = new Thread(this); // pass the thread a runnable object (this)
			animationThread.start();
		}
	}

	// implements runnable - must have run method
	public void run() {

		// lower this thread's priority; let the "main" aka 'Event Dispatch'
		// thread do what it needs to do first
		animationThread.setPriority(Thread.MIN_PRIORITY);

		// and get the current time
		long lStartTime = System.currentTimeMillis();

		// this thread animates the scene
		while (Thread.currentThread() == animationThread) {
			spawnNewShipFloater();
			spawnShieldFloater();
			spawnMissileFloater();
			spawnExplodeFloater();
			spawnEvilBullets();
			if(CommandCenter.getInstance().getHyper()<100){
				CommandCenter.getInstance().setHyper(CommandCenter.getInstance().getHyper()+1);
			}
			gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must 
														// surround the sleep() in a try/catch block
														// this simply controls delay time between 
														// the frames of the animation

			checkCollisions();
			checkNewLevel();

			try {
				// The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update) 
				// between frames takes longer than ANI_DELAY, then the difference between lStartTime - 
				// System.currentTimeMillis() will be negative, then zero will be the sleep time
				lStartTime += ANI_DELAY;
				Thread.sleep(Math.max(0,
						lStartTime - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				// do nothing (bury the exception), and just continue, e.g. skip this frame -- no big deal
			}
		} // end while
	} // end run

	private void checkCollisions() {
		Point pntFriendCenter, pntFoeCenter;
		int radFriend, radFoe;

		//This has order-of-growth of O(n^2), there is no way around this.
		for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
			for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {

				pntFriendCenter = movFriend.getCenter();
				pntFoeCenter = movFoe.getCenter();
				radFriend = movFriend.getRadius();
				radFoe = movFoe.getRadius();

				//detect collision
				if (pntFriendCenter.distance(pntFoeCenter) < (radFriend + radFoe)) {
					//remove the friend (so long as he is not protected)
					if (!movFriend.isProtected()){
						CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
					}

						//if we hit an evil falcon, need to check how many hits have been made and how many need to be hit until it dies
						if(movFoe instanceof EvilFalcon){
							((EvilFalcon) movFoe).incHitsMade();
							if(((EvilFalcon) movFoe).getHitsMade()==((EvilFalcon) movFoe).getHitsNeeded()){
								createDebris(CommandCenter.getInstance().getEvilFalcon(),15);
								CommandCenter.getInstance().setScore(CommandCenter.getInstance().getScore()+1000);
								CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);
								CommandCenter.getInstance().setEvilFalcon(null);
							}
						}
						else {
							createDebris(movFoe,4);
							CommandCenter.getInstance().setScore(CommandCenter.getInstance().getScore() + 100);
							CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);
						}
						Sound.playSound("kapow.wav");
				 }

				}//end if 
			}//end inner for

		//check for collisions between falcon and floaters
		if (CommandCenter.getInstance().getFalcon() != null){
			Point pntFalCenter = CommandCenter.getInstance().getFalcon().getCenter();

			int radFalcon = CommandCenter.getInstance().getFalcon().getRadius();
			Point pntFloaterCenter;
			int radFloater;
			
			for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
				pntFloaterCenter = movFloater.getCenter();
				radFloater = movFloater.getRadius();
	
				//detect collision
				if (pntFalCenter.distance(pntFloaterCenter) < (radFalcon + radFloater)) {

					CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);
					//increase number of falcons
					if(movFloater instanceof NewShipFloater){
						CommandCenter.getInstance().setNumFalcons(CommandCenter.getInstance().getNumFalcons()+1);
					}

					//set shield capacity
					if(movFloater instanceof ShieldFloater){
						CommandCenter.getInstance().getFalcon().setShield(100);
					}

					//set mega bullets
					if(movFloater instanceof MegaMissileFloater){
						CommandCenter.getInstance().getFalcon().setMissile(100);
					}

					//increase number of explode floaters
					if(movFloater instanceof ExplodeFloater){
						if(CommandCenter.getInstance().getExplode()<10){
							CommandCenter.getInstance().setExplode(CommandCenter.getInstance().getExplode()+1);
						}
					}

					Sound.playSound("pacman_eatghost.wav");
	
				}//end if 
			}//end inner for
		}//end if not null

		processGameOpsQueue();

	}//end meth

	private void processGameOpsQueue() {

		//deferred mutation: these operations are done AFTER we have completed our collision detection to avoid
		// mutating the movable arraylists while iterating them above
		while(!CommandCenter.getInstance().getOpsList().isEmpty()){
			CollisionOp cop =  CommandCenter.getInstance().getOpsList().dequeue();
			Movable mov = cop.getMovable();
			CollisionOp.Operation operation = cop.getOperation();

			switch (mov.getTeam()){
				case FOE:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFoes().add(mov);
					} else {
						CommandCenter.getInstance().getMovFoes().remove(mov);
						if (mov instanceof Asteroid){
							spawnSmallerAsteroids((Asteroid) mov);
						}
					}

					break;
				case FRIEND:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFriends().add(mov);
					} else {
						CommandCenter.getInstance().getMovFriends().remove(mov);
						if (mov instanceof Falcon)
							CommandCenter.getInstance().spawnFalcon();
					}
					break;

				case FLOATER:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFloaters().add(mov);
					} else {
						CommandCenter.getInstance().getMovFloaters().remove(mov);
					}
					break;

				case DEBRIS:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovDebris().add(mov);
					} else {
						CommandCenter.getInstance().getMovDebris().remove(mov);
					}
					break;

			}

		}
	}

	private void spawnSmallerAsteroids(Asteroid originalAsteroid) {
			//big asteroid
		if(originalAsteroid.getSize() == 0){
			//spawn two medium Asteroids
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);
		}
			//medium size asteroid exploded
		else if(originalAsteroid.getSize() == 1){
			//spawn three small Asteroids
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);
		}
			//if it's a small asteroid, do nothing.
	}

	//create random number of debris for each explosion
	private void createDebris(Movable movFoe, int min){
		int numOfDebris = Game.R.nextInt(7) + min;
		for (int i = 0; i < numOfDebris; i++) {
			CommandCenter.getInstance().getOpsList().enqueue(new Debris(movFoe.getCenter()), CollisionOp.Operation.ADD);
		}
	}

	//for when we have an explosion, get rid of everything on screen
	private void clearScreen(){
		List<Movable> foes = CommandCenter.getInstance().getMovFoes();
		//List<Point> centers = foes.stream().map(m -> m.getCenter()).collect(Collectors.toList());
		for (Movable foe: foes) {
			createDebris(foe,10);
			CommandCenter.getInstance().setScore(CommandCenter.getInstance().getScore()+300);
		}
		CommandCenter.getInstance().getMovFoes().clear();
		CommandCenter.getInstance().setEvilFalcon(null);
		CommandCenter.getInstance().setExplode(0);
		CommandCenter.getInstance().getFalcon().setShield(100);
		CommandCenter.getInstance().getFalcon().setMissile(100);
	}

	private void spawnNewShipFloater() {
		//appears more often as your level increses.
		if ((System.currentTimeMillis() / ANI_DELAY) % (SPAWN_NEW_SHIP_FLOATER - level * 7L) == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new NewShipFloater(), CollisionOp.Operation.ADD);
		}
	}

	private void spawnShieldFloater() {
		//appears more often as your level increses.
		if ((System.currentTimeMillis() / ANI_DELAY) % (SPAWN_NEW_SHIELD_FLOATER - level * 6L) == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new ShieldFloater(), CollisionOp.Operation.ADD);
		}
	}

	private void spawnMissileFloater() {
		//appears more often as your level increses.
		if ((System.currentTimeMillis() / ANI_DELAY) % (SPAWN_NEW_MISSILE_FLOATER - level * 8L) == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new MegaMissileFloater(), CollisionOp.Operation.ADD);
		}
	}

	private void spawnExplodeFloater() {
		//appears more often as your level increses.
		if ((System.currentTimeMillis() / ANI_DELAY) % (SPAWN_NEW_EXPLODE_FLOATER - level * 7L) == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new ExplodeFloater(), CollisionOp.Operation.ADD);
		}
	}

	//make evil bullets randomly
	public void spawnEvilBullets(){
		if(CommandCenter.getInstance().getEvilFalcon()!= null &&
		CommandCenter.getInstance().getEvilFalcon().getHitsMade()!=CommandCenter.getInstance().getEvilFalcon().getHitsNeeded() &&
				(System.currentTimeMillis() / ANI_DELAY) % (Math.max(10,25-level)) == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(
					new EvilBullet(CommandCenter.getInstance().getEvilFalcon(),
					CommandCenter.getInstance().getFalcon()),CollisionOp.Operation.ADD);
			CommandCenter.getInstance().getOpsList().enqueue(
					new EvilBullet(CommandCenter.getInstance().getEvilFalcon(),
							CommandCenter.getInstance().getFalcon()),CollisionOp.Operation.ADD);
		}
	}


	// Called when user presses 's'
	private void startGame() {
		CommandCenter.getInstance().clearAll();
		CommandCenter.getInstance().initGame();
		CommandCenter.getInstance().setLevel(0);
		CommandCenter.getInstance().setExplode(0);
		CommandCenter.getInstance().setPlaying(true);
		CommandCenter.getInstance().setPaused(false);

	}

	//this method spawns new asteroids
	private void spawnAsteroids(int nNum) {
		for (int nC = 0; nC < nNum; nC++) {
			//Asteroids with size of zero are big
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(0), CollisionOp.Operation.ADD);

		}
	}
	
	private boolean isLevelClear(){
		//if there are no more Asteroids on the screen
		boolean asteroidFree = true;
		for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
			if (movFoe instanceof Asteroid || movFoe instanceof EvilFalcon){
				asteroidFree = false;
				break;
			}
		}
		return asteroidFree;
	}

	//if level 3,6,9 etc, spawn an evil falcon enemy
	private void checkNewLevel(){
		if (isLevelClear() && CommandCenter.getInstance().isPlaying()){
			if (CommandCenter.getInstance().getFalcon() !=null)
			//more asteroids at each level to increase difficulty
				if(CommandCenter.getInstance().getLevel()>0 && (CommandCenter.getInstance().getLevel()+1)%3==0){
					CommandCenter.getInstance().spawnEvilFalcon();
					spawnAsteroids((CommandCenter.getInstance().getLevel()/3)-1);
				}
			else {
					spawnAsteroids(CommandCenter.getInstance().getLevel() + 2);
				}
			CommandCenter.getInstance().setLevel(CommandCenter.getInstance().getLevel() + 1);
		}
	}


	// Varargs for stopping looping-music-clips
	private static void stopLoopingSounds(Clip... clpClips) {
		for (Clip clp : clpClips) {
			clp.stop();
		}
	}

	// ===============================================
	// KEYLISTENER METHODS
	// ===============================================

	@Override
	public void keyPressed(KeyEvent e) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		int nKey = e.getKeyCode();

		if (nKey == START && !CommandCenter.getInstance().isPlaying())
			startGame();

		if (fal != null) {
			switch (nKey) {
			case PAUSE:
				CommandCenter.getInstance().setPaused(!CommandCenter.getInstance().isPaused());
				if (CommandCenter.getInstance().isPaused())
					stopLoopingSounds(clpMusicBackground, clpThrust);
				break;
			case QUIT:
				System.exit(0);
				break;
			case UP:
				fal.thrustOn();
				if (!CommandCenter.getInstance().isPaused())
					clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case LEFT:
				fal.rotateLeft();
				break;
			case RIGHT:
				fal.rotateRight();
				break;
			//if f is pressed, then if enough explode floaters have been collected, clear screen
			case EXPLODE:
				if(CommandCenter.getInstance().getExplode()==10){
					clearScreen();
				}
				break;
			//if hyper jump capabilities at 100 then move to random location
			case HYPER:
				if(CommandCenter.getInstance().getHyper()==100) {
					CommandCenter.getInstance().getFalcon().setCenter(
							new Point((R.nextInt(Game.DIM.width-100)+50), (R.nextInt(Game.DIM.height-100)+50)));
					CommandCenter.getInstance().setHyper(0);
				}
				break;
			// if we have a shield, then turn shield on
			case SHIELD:
				if(CommandCenter.getInstance().getFalcon().getShield()>0){
					CommandCenter.getInstance().getFalcon().shieldSwitch();
				}
				break;
			// case NUM_ENTER:

			default:
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		int nKey = e.getKeyCode();
		//show the key-code in the console
		 System.out.println(nKey);

		if (fal != null) {
			switch (nKey) {
			case FIRE:
				CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal), CollisionOp.Operation.ADD);
				Sound.playSound("laser.wav");
				break;
			case LEFT:
				fal.stopRotating();
				break;
			case RIGHT:
				fal.stopRotating();
				break;
			case UP:
				fal.thrustOff();
				clpThrust.stop();
				break;
			case MUTE:
				if (!muted){
					stopLoopingSounds(clpMusicBackground);
				} 
				else {
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
				}
				muted = !muted;
				break;


			default:
				break;
			}
		}
	}

	@Override
	// does nothing, but we need it b/c of KeyListener contract
	public void keyTyped(KeyEvent e) {
	}

}


