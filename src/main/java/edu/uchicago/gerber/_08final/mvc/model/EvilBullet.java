package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//evil bullet tracks down falcon
public class EvilBullet extends Sprite {

    private final double FIRE_POWER = 25.0;

    public EvilBullet(EvilFalcon evilFal, Falcon fal) {

        super();
        setTeam(Team.FOE);


        //a bullet expires after 20 frames
        setExpiry(20);
        setRadius(6);
        setColor(Color.magenta);

        //find angle from evil falcon to falcon
        double angle = Math.atan2(evilFal.getCenter().y-fal.getCenter().y,evilFal.getCenter().x-fal.getCenter().x);

        //bullets more accurate as level increases
        angle = (angle + (1-0.01*CommandCenter.getInstance().getLevel())*(new Random().nextDouble()-0.5)) + Math.PI;

        setDeltaX(
                Math.cos((angle)) * FIRE_POWER);
        setDeltaY(
                Math.sin((angle)) * FIRE_POWER);
        setCenter(evilFal.getCenter());

        //set the bullet orientation to point to falcon
        setOrientation((int) (Math.toDegrees(angle)));

        //make sure to setCartesianPoints last
        //defined the points on a cartesian grid
        List<Point> pntCs = new ArrayList<>();

        pntCs.add(new Point(0, 3)); //top point

        pntCs.add(new Point(1, -1));
        pntCs.add(new Point(0, -2));
        pntCs.add(new Point(-1, -1));
        setCartesians(pntCs);
    }


    @Override
    public void move() {
        super.move();
        expire();
    }
}
