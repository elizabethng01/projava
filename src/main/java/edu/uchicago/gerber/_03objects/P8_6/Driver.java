package edu.uchicago.gerber._03objects.P8_6;

public class Driver {
    public static void main(String[] args) {
        Car lightningMcqueen = new Car(30);
        lightningMcqueen.addGas(2);
        lightningMcqueen.drive(46);
        System.out.println(lightningMcqueen.getGasLevel());
    }
}
