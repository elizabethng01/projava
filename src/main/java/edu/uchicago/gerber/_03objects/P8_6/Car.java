package edu.uchicago.gerber._03objects.P8_6;

public class Car {
    private int MPG;
    private double tank;

    public Car(int MPG){
        this.MPG = MPG;
        this.tank=0;
    }

    public void addGas(double gallons){
        this.tank+=gallons;
    }

    public void drive(double distance){
        double gallonsUsed = distance/MPG;
        this.tank-=gallonsUsed;
    }

    //give gas level to 2 decimal places
    public double getGasLevel(){
        return ((double)Math.round(tank*100))/100;
    }

}
