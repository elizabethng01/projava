package edu.uchicago.gerber._03objects.P8_14;

import java.util.ArrayList;
import java.util.Scanner;

//largest area, largest population, largest population density

public class Driver {

    public static String largestArea(ArrayList<Country> countries){
        //store index of country with largest area, initialise to index 0
        int maxArea = 0;
        //if any country has a larger area, update max area
        for (int i = 1; i<countries.size();i++){
            if(countries.get(i).getArea()>countries.get(maxArea).getArea()){
                maxArea = i;
            }
        }
        return "The country with the largest area is " + countries.get(maxArea).getName() + " with area " + countries.get(maxArea).getArea();
    }

    public static String largestPop(ArrayList<Country> countries){
        //store index of country with largest population, initialise to index 0
        int maxPop = 0;
        for (int i = 1; i<countries.size();i++){
            if(countries.get(i).getPopulation()>countries.get(maxPop).getPopulation()){
                maxPop = i;
            }
        }
        return "The country with the largest population is " + countries.get(maxPop).getName() + " with population " + countries.get(maxPop).getPopulation();
    }

    public static String largestPopDensity(ArrayList<Country> countries){
        //store index of country with largest population density, initialise to index 0
        int maxPD = 0;
        for (int i = 1; i<countries.size();i++){
            if(countries.get(i).popDensity()>countries.get(maxPD).popDensity()){
                maxPD = i;
            }
        }
        return "The country with the largest population density is " + countries.get(maxPD).getName() + " with area " + countries.get(maxPD).popDensity();
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        ArrayList<Country> countries = new ArrayList<Country>();
        System.out.println("Enter a country's name, population, and area. Type DONE when you are done");

        String newIn = in.nextLine();
        while(!newIn.equals("DONE")){
            String[] countryStats = newIn.split(" ");
            //if not 3 inputs, then prompt user for input again
            if (countryStats.length!=3){
                System.out.println("Your input was incorrect. Please enter a name, population, and area.");
            }
            else {
                //if a string and two integers are not provided then prompt user for input again
                try{
                    countries.add(new Country(countryStats[0],Integer.parseInt(countryStats[1]),Integer.parseInt(countryStats[2])));
                } catch (NumberFormatException e) {
                    System.out.println("Your input was incorrect. Please enter a name, population, and area.");
                }
            }
            newIn=in.nextLine();
        }

        System.out.println(largestPop(countries));
        System.out.println(largestArea(countries));
        System.out.println(largestPopDensity(countries));

    }
}
