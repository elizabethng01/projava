package edu.uchicago.gerber._03objects.P8_14;

public class Country {
    //name, population, area
    //write program that reads in a set of countries and prints
    //largest area, largest population, largest population density
    private String name;
    private int population;
    private int area;

    public Country(String name, int population, int area){
        this.name = name;
        this.population = population;
        this.area = area;
    }

    public int getArea() {
        return area;
    }

    public int getPopulation() {
        return population;
    }

    public String getName() {
        return name;
    }

    //population density = population/area
    public double popDensity(){
        return ((double) population)/area;
    }

}
