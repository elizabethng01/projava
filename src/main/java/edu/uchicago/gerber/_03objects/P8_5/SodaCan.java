package edu.uchicago.gerber._03objects.P8_5;

public class SodaCan {
    private double height;
    private double radius;

    public SodaCan(double height, double radius){
        this.height=height;
        this.radius=radius;
    }

    public SodaCan(int height, int radius){
        this.height = (double) height;
        this.radius = (double) radius;
    }

    //formula = 2*pi*r(r+h)
    public double getSurfaceArea(){
        return 2*Math.PI*radius*(height+radius);
    }

    //formula = pi*r*r*h
    public double getVolume(){
        return Math.PI*radius*radius*height;
    }
}
