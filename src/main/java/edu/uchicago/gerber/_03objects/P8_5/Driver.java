package edu.uchicago.gerber._03objects.P8_5;

public class Driver {
    public static void main(String[] args) {
        SodaCan canny = new SodaCan(5,3);
        System.out.println(canny.getSurfaceArea());
        System.out.println(canny.getVolume());
    }
}
