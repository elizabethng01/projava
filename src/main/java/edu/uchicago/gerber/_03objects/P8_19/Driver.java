package edu.uchicago.gerber._03objects.P8_19;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a starting angle (rads) and initial velocity (m/s): ");
        double angle = 0;
        double velocity = 0;
        try {
            angle = in.nextDouble();
            velocity = in.nextDouble();

        }
        //if incorrect input then end
        catch (InputMismatchException e){
            System.out.println("Your input was incorrect");
        }


        Cannonball canny = new Cannonball(0);
        canny.shoot(angle,velocity);
    }
}
