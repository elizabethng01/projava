package edu.uchicago.gerber._03objects.P8_19;

public class Cannonball {
    private double xPos;
    private double yPos=0;
    private double xVel=0;
    private double yVel=0;

    public Cannonball(double xPos){
        this.xPos=xPos;
    }

    public void move(double sec){
        xPos += sec*xVel;
        yPos += sec*yVel-(0.5*9.81*sec*sec);
        yVel -= 9.81*sec;
    }
    //give x position to 2 decimal places
    public double getX(){
        return ((double)Math.round(xPos*100)/100);
    }
    //give y position to 2 decimal places
    public double getY(){
        return ((double)Math.round(yPos*100)/100);
    }
    //angle in rads, velocity in m/s
    public void shoot(double angle, double velocity){
        xVel = velocity * Math.cos(angle);
        yVel = velocity * Math.sin(angle);
        double time = 0.1;
        double totalTime = 0.1;
        this.move(time);
        while (yPos>0){
            System.out.println("After " + totalTime + " seconds, the position of the cannonball is: (" + this.getX() + "," + this.getY()+")");
            //give total time to 1 decimal place
            totalTime= ((double) Math.round((totalTime+time)*10))/10;
            this.move(time);
        }
    }

}
