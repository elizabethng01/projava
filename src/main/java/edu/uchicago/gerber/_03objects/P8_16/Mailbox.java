package edu.uchicago.gerber._03objects.P8_16;

import java.util.ArrayList;

public class Mailbox extends Message {

    private ArrayList<Message> messages;

    public Mailbox(){
        this.messages = new ArrayList<Message>();
    }

    public void addMessage(Message m){
        this.messages.add(m);
    }

    public Message getMessage(int i){
        return messages.get(i);
    }

    public void removeMessage(int i){
        messages.remove(i);
    }
}
