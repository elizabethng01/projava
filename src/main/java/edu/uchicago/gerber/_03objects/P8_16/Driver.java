package edu.uchicago.gerber._03objects.P8_16;

public class Driver {
    public static void main(String[] args) {
        Mailbox maily = new Mailbox();
        Message m1 = new Message("me","you","hello");
        m1.append("this is message 1");
        maily.addMessage(m1);
        Message m2 = new Message("a","b");
        m2.append("more message 2");
        maily.addMessage(new Message("a","b"));
        System.out.println(maily.getMessage(0));
        System.out.println(maily.getMessage(1));
        maily.removeMessage(0);
        System.out.println(maily.getMessage(0));

    }
}
