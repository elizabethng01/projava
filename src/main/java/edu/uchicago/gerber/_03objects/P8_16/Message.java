package edu.uchicago.gerber._03objects.P8_16;

import java.util.ArrayList;

public class Message {
    private String recipient;
    private String sender;
    private ArrayList<String> message;

    public Message(){
        this.recipient = "";
        this.sender = "";
        this.message = new ArrayList<String>();
    }
    public Message(String sender, String recipient){
        this.sender=sender;
        this.recipient=recipient;
        this.message = new ArrayList<String>();
    }

    public Message(String sender, String recipient, String text){
        this.sender=sender;
        this.recipient=recipient;
        this.message = new ArrayList<String>();
        message.add(text);
    }

    public void append(String text){
        message.add(text);
    }

    public String toString(){
        StringBuilder messageString = new StringBuilder();
        messageString.append("From: " + sender + "%nTo: " + recipient);
        for (String line:message) {
            messageString.append("%n");
            messageString.append(line);
        }
        return messageString.toString();
    }

}
