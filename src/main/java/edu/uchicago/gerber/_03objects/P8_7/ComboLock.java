package edu.uchicago.gerber._03objects.P8_7;

public class ComboLock {
    private int code1;
    private int code2;
    private int code3;
    private int turnNum; //store how many times lock has been turned
    private int current; //to store number the lock is currently pointing to
    private boolean mistakes = false; //flag for mistakes

    public ComboLock(int code1, int code2, int code3){
        this.code1=code1;
        this.code2=code2;
        this.code3=code3;
        this.turnNum=0;
        this.current=0;
    }

    //reset lock to 0 and start attempt again
    public void reset(){
        this.current=0;
        this.turnNum=0;
    }

    public void turnLeft(int ticks){
        current+=ticks;
        if(current>0){
            current=current-40;
        }
        turnNum++;
        //if it isn't the second turn or if it is the second turn but not the right number, a mistake has been made
        if(turnNum!=2 || current!=code2){
            this.mistakes=true;
        }
    }

    public void turnRight(int ticks){
        current-=ticks;
        if(current<0){
            current=40+current;
        }
        turnNum++;
        //if it is the second turn or if it is the first turn but not the first code or the third turn but not the third code then a mistake has been made
        if((turnNum==2)||(turnNum==1 && current!=code1)||turnNum==3 && current!=code3){
            this.mistakes=true;
        }
    }

    //if no mistakes made, then the lock will open
    public boolean open(){
        return !mistakes;
    }
}
