package edu.uchicago.gerber._03objects.P8_7;

public class Driver {
    public static void main(String[] args) {
        ComboLock locky = new ComboLock(30,23,17);
        locky.reset();
        locky.turnRight(10);
        locky.turnLeft(33);
        locky.turnRight(6);
        System.out.println(locky.open());
    }
}
