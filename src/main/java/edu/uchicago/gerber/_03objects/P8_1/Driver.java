package edu.uchicago.gerber._03objects.P8_1;

public class Driver {
    public static void main(String[] args) {
        Microwave mine = new Microwave();
        mine.increase30();
        mine.switchPower();
        mine.start();
    }
}
