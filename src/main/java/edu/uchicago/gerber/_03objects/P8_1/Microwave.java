package edu.uchicago.gerber._03objects.P8_1;

public class Microwave {

    private int seconds;
    private int powerLevel;

    public Microwave(){
        this.seconds=0;
        this.powerLevel=1;
    }

    public void reset(){
        this.seconds=0;
    }

    public void start(){
        System.out.println("Cooking for " + seconds + " seconds at level " + powerLevel);
        this.seconds=0;
    }

    public void switchPower(){
        if (this.powerLevel==1){
            this.powerLevel=2;
        }
        else{
            this.powerLevel=1;
        }
    }

    public void increase30(){
        this.seconds+=30;
    }

    public void addTime(int time){
        this.seconds+=time;
    }

}
