package edu.uchicago.gerber._03objects.P8_8;

public class Driver {
    public static void main(String[] args) {
        VotingMachine votey = new VotingMachine();
        votey.voteDemocrat();
        votey.voteRepublican();
        System.out.println(votey.democratTally());
        System.out.println(votey.democratRepublican());
    }
}
