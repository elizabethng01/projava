package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachine {

    private int democrat;
    private int republican;

    public VotingMachine(){
        this.democrat=0;
        this.republican=0;
    }

    public VotingMachine(int democrat, int republican){
        this.democrat = democrat;
        this.republican = republican;
    }

    public void clear(){
        this.democrat=0;
        this.republican=0;
    }

    public void voteDemocrat(){
        this.democrat+=1;
    }
    public void voteRepublican(){
        this.republican+=1;
    }
    public int democratTally(){
        return democrat;
    }
    public int democratRepublican(){
        return republican;
    }

}
