package edu.uchicago.gerber._05dice.P10_19;
/*
Write a graphical application that produces a restaurant bill. Provide buttons for ten popular dishes or drink items.
(You decide on the items and their prices.) Provide text fields for entering less popular items and prices.
In a text area, show the bill, including tax and a suggested tip.
*/

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BillFrame extends JFrame {
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT= 700;

    private static final double TAX_RATE = 0.1025;
    private static final double TIP_RATE = 0.18;
    private static final String DEFAULT_LABEL = "New Item";
    private static final String DEFAULT_VALUE = "0.0";

    private JLabel billLabel;

    //display item total
    private double itemTotal;
    private JLabel itemTotalLabel;
    private JTextField itemTotalField;

    //display total bill
    private double total;
    private JLabel totalLabel;
    private JTextField totalField;

    //display tax rate
    private double tax;
    private JLabel taxLabel;
    private JTextField taxField;

    //display tip
    private double tip;
    private JLabel tipLabel;
    private JTextField tipField;

    //custom items
    private JTextField item1;
    private JTextField price1;
    private JButton add1;

    private JTextField item2;
    private JTextField price2;
    private JButton add2;

    private JTextField item3;
    private JTextField price3;
    private JButton add3;

    public BillFrame(){
        billLabel = new JLabel("BILL",SwingConstants.CENTER);

        itemTotalLabel = new JLabel("ITEM TOTAL");
        itemTotalField = new JTextField(String.valueOf(itemTotal));

        taxLabel = new JLabel("TAX (10.25%)");
        taxField = new JTextField(String.valueOf(tax));

        tipLabel = new JLabel("TIP (18%)");
        tipField = new JTextField(String.valueOf(tip));

        totalLabel = new JLabel("TOTAL");
        totalField = new JTextField(String.valueOf(total));

        taxField.setEditable(false);
        tipField.setEditable(false);
        totalField.setEditable(false);
        itemTotalField.setEditable(false);

        //initialise custom items
        item1 = new JTextField(DEFAULT_LABEL);
        price1 = new JTextField(DEFAULT_VALUE);
        add1 = new JButton("ADD");
        ActionListener add1Listener = new NewItemListener(price1);
        add1.addActionListener(add1Listener);
        item2 = new JTextField(DEFAULT_LABEL);
        price2 = new JTextField(DEFAULT_VALUE);
        add2 = new JButton("ADD");
        ActionListener add2Listener = new NewItemListener(price2);
        add2.addActionListener(add2Listener);
        item3 = new JTextField(DEFAULT_LABEL);
        price3 = new JTextField(DEFAULT_VALUE);
        add3 = new JButton("ADD");
        ActionListener add3Listener = new NewItemListener(price3);
        add3.addActionListener(add3Listener);

        createMenuPanel();
        createTotalPanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);

    }

    //create multiple menu buttons with same action listener
    public JButton makeMenuItem(String item, String price){
        JButton button = new JButton(item + "   " + price);
        ActionListener listener = new ItemButtonListener(price);
        button.addActionListener(listener);
        return button;
    }

    //action performed method for custom menu items
    class NewItemListener implements ActionListener {
        private JTextField price;
        public NewItemListener(JTextField price){
            this.price = price;
        }
        //update item total, tip, tax, and bill total
        public void actionPerformed(ActionEvent event){
            itemTotal += Double.parseDouble(price.getText());
            tax = (double) Math.round(itemTotal *100 * TAX_RATE)/100;
            tip = (double) Math.round(itemTotal * 100 * TIP_RATE)/100;
            total = (double) Math.round((itemTotal + tax + tip)*100)/100;
            itemTotalField.setText(String.valueOf(itemTotal));
            totalField.setText(String.valueOf(total));
            taxField.setText(String.valueOf(tax));
            tipField.setText(String.valueOf(tip));
        }
    }

    //action performed method for popular menu items
    class ItemButtonListener implements ActionListener {
        private Double price;
        public ItemButtonListener(String price){
            this.price = Double.valueOf(price);
        }
        //update item total, tip, tax, and bill total
        public void actionPerformed(ActionEvent event){
            itemTotal += price;
            tax = (double) Math.round(itemTotal *100 * TAX_RATE)/100;
            tip = (double) Math.round(itemTotal * 100 * TIP_RATE)/100;
            total = (double) Math.round((itemTotal + tax + tip)*100)/100;
            itemTotalField.setText(String.valueOf(itemTotal));
            totalField.setText(String.valueOf(total));
            taxField.setText(String.valueOf(tax));
            tipField.setText(String.valueOf(tip));
        }
    }

    private JPanel newItemPanel(JTextField item, JTextField price, JButton add){
        JPanel newItemPanel = new JPanel();
        newItemPanel.setLayout(new GridLayout(1,3));
        newItemPanel.add(item);
        newItemPanel.add(price);
        newItemPanel.add(add);
        return newItemPanel;
    }

    private void createMenuPanel(){
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new GridLayout(14,1));
        menuPanel.add(billLabel);
        menuPanel.add(makeMenuItem("Sausage Pizza", "14.50"));
        menuPanel.add(makeMenuItem("Cheese Pizza", "12.00"));
        menuPanel.add(makeMenuItem("Pepperoni Pizza", "17.25"));
        menuPanel.add(makeMenuItem("Large Fries", "6.50"));
        menuPanel.add(makeMenuItem("Veggie Pizza", "13.75"));
        menuPanel.add(makeMenuItem("Large Milkshake", "9.50"));
        menuPanel.add(makeMenuItem("Hawaiian Pizza", "10.00"));
        menuPanel.add(makeMenuItem("Garlic Knots", "8.25"));
        menuPanel.add(makeMenuItem("Meat Lovers Pizza", "16.50"));
        menuPanel.add(makeMenuItem("XL Everything Pizza", "32.25"));
        menuPanel.add(newItemPanel(item1,price1,add1));
        menuPanel.add(newItemPanel(item2,price2,add2));
        menuPanel.add(newItemPanel(item3,price3,add3));
        add(menuPanel, BorderLayout.CENTER);
    }

    private void createTotalPanel(){
        JPanel totalPanel = new JPanel();
        totalPanel.setLayout(new GridLayout(4,2));
        totalPanel.add(itemTotalLabel);
        totalPanel.add(itemTotalField);
        totalPanel.add(taxLabel);
        totalPanel.add(taxField);
        totalPanel.add(tipLabel);
        totalPanel.add(tipField);
        totalPanel.add(totalLabel);
        totalPanel.add(totalField);

        add(totalPanel, BorderLayout.SOUTH);
    }

}
