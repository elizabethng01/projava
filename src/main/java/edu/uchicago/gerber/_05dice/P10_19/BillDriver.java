package edu.uchicago.gerber._05dice.P10_19;

import javax.swing.JFrame;

public class BillDriver {
    public static void main(String[] args) {
        JFrame frame = new BillFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Bill Calculator");
        frame.setVisible(true);
    }
}
