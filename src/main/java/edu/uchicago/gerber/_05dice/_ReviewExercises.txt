#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################



R10.5 Events
An event object holds information about the event that has occurred, such as a mouse click or a key being pressed on
the keyboard.
An event source is the user-interface component that generates a particular event, such as the mouse or a button.
An event listener belongs to a class whose methods decide the actions that will take place when an event occurs.

R10.6 actionPerformed
The actionPerformed method of an event listener is called by the event source when the event occurs.

R10.11 Inner class and event-listeners
Inner classes are used for event listeners as they give the event listeners access to instance variables and methods
of the outer class. Since listener classes tend to be very short, it is more convenient to have the event listener as
an inner class rather than creating a separate file to store the class.
It is still possible to implement event listeners without using inner classes however the listener would need to be
constructed with a reference to the label field.

R10.14 Object hierarchies
A method declared in JTextArea: append
A method that JTextArea inherits from JTextComponent: setText, setEditable
A method that JTextArea inherits from JComponent: setVisible, update


R10.22 Graphic methods
Instead of having g.setColor(Color.GREEN) and g.setColor(Color.RED), we could replace Color.GREEN and Color.RED with
the colour we would like on our flag instead


R11.2 Layout managers
Using a layout manager makes it easier to adjust and move components, and will maintain the desired layout even if the
user interface window is manipulated. It is also helpful as it gives visual guidelines as to what the finished product
will look like.

R11.11 ButtonGroup
Radio buttons require a button group as only one can be turned on at any time, so we must group them to know which other
radio buttons should be turned off when one of them is on. Check boxes do not need a button group as the state of each
check box is independent of other check boxes and thus check boxes do not need to be placed in a button group as they
do not exclude each other.

R11.19 Types of Events
An ActionEvent is when a component defined action has occurred, such as pressing a button.
A MouseEvent is a notification of any mouse action has occurred, such as pressing or releasing the mouse or moving into
a specified area.

R11.20 Events
What information does an action event object carry? What additional information does a mouse event object carry?
Hint: Check the API documentation.
An action event object carries all the fields inherited from the AWTEvent class. On top of the AWTEvent class fields, a
mouse event also carries fields from the ComponentEvent class and the InputEvent class, as well as information
regarding mouse specific actions, such as mouse_clicked, mouse_dragged, mouse_entered etc.

R11.21 ActionListener versus MouseListener
The ActionListener only has one method to track when an action has been performed, such as when a button is clicked, as
there is only one function that our program is concerned with for each event source.
The MouseListener has five methods to track the various functions of the mouse. In particular, we must provide a method
for when the mouse has been clicked, when the mouse has been released, when the mouse has been clicked, when the mouse
has entered a component and when the mouse exits a component.