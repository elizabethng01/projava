package edu.uchicago.gerber._05dice.P10_9;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class FlagDriver {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(300,400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component = new HorizontalFlagComponent();
        frame.add(component);

        frame.setVisible(true);
    }
}
