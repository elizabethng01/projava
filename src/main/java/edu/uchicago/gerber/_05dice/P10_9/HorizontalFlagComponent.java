package edu.uchicago.gerber._05dice.P10_9;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;

public class HorizontalFlagComponent extends JComponent {

    public static final Color Dark_Green = new Color(0,102,0);
    public void paintComponent(Graphics g){
        //german flag
        drawHorizontalFlag(g,10,10,100,Color.black,Color.red,Color.yellow);
        //hungarian flag
        drawHorizontalFlag(g,10,125,100,Color.red,Color.white,Dark_Green);
    }

    void drawHorizontalFlag(Graphics g, int xLeft, int yTop, int height, Color top, Color middle, Color bottom){
        g.setColor(top);
        g.fillRect(xLeft,yTop,height*3/2,height/3);
        g.setColor(middle);
        g.fillRect(xLeft,yTop+height/3,height*3/2,height/3);
        g.setColor(bottom);
        g.fillRect(xLeft,yTop+2*height/3,height*3/2,height/3);
    }

}
