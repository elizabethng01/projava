package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;

public class ClickCircleDriver {
    public static void main(String[] args) {
        JFrame frame = new ClickCircleFrame();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
