package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ClickCircleFrame extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 400;

    private ClickCircleComponent clickCircle;

    private int xCenter;
    private int yCenter;
    private int xEdge;
    private int yEdge;
    boolean centerClicked = false;

    public ClickCircleFrame(){
        clickCircle = new ClickCircleComponent();
        add(clickCircle);
        MouseListener listener = new CircleMouseListener();
        clickCircle.addMouseListener(listener);

        setSize(FRAME_WIDTH,FRAME_HEIGHT);
    }

    //if the center has been clicked, then draw circle
    //if center has not been clicked, set center
    public class CircleMouseListener implements MouseListener {
        public void mousePressed(MouseEvent event){
            if(centerClicked){
                xEdge = event.getX();
                yEdge = event.getY();
                clickCircle.drawNewCircle(xCenter,yCenter,xEdge,yEdge);
            }
            else{
                xCenter = event.getX();
                yCenter = event.getY();
            }
            centerClicked=!centerClicked;

        }
        public void mouseReleased(MouseEvent event){}
        public void mouseClicked(MouseEvent event){}
        public void mouseEntered(MouseEvent event){}
        public void mouseExited(MouseEvent event){}
    }
}
