package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import java.awt.*;

/*
Write a program that allows the user to specify a circle with two mouse presses, the first one on the center and the
second on a point on the periphery.
Hint: In the mouse press handler, you must keep track of whether you already received the center point in a previous
mouse press.
 */

import javax.swing.JComponent;
import java.util.Random;

public class ClickCircleComponent extends JComponent {
    int xCenter;
    int yCenter;
    int xEdge;
    int yEdge;

    public void paintComponent(Graphics g) {
        drawCircle(g, xCenter, yCenter, xEdge, yEdge);
    }

    void drawCircle(Graphics g, int xCenter, int yCenter, int xEdge, int yEdge) {
        //choose random colour to draw circle
        Random rand = new Random();
        float red = rand.nextFloat();
        float green = rand.nextFloat();
        float blue = rand.nextFloat();
        Color randomColor = new Color(red,green,blue);
        g.setColor(randomColor);

        //find radius by finding distance between first and second points
        int radius = (int) Math.sqrt(Math.pow((xEdge - xCenter), 2) + Math.pow((yEdge - yCenter), 2));
        g.fillOval(xCenter - (radius), yCenter - (radius), radius*2, radius*2);
    }

    public void drawNewCircle(int xCenter, int yCenter, int xEdge, int yEdge){
        this.xCenter = xCenter;
        this.yCenter = yCenter;
        this.xEdge = xEdge;
        this.yEdge = yEdge;
        repaint();
    }


}
