package edu.uchicago.gerber._05dice.pig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PigFrame extends JFrame {
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT= 300;
    private static final String DEFAULT_VALUE = "0";

    private JLabel title;

    private int playerScore = 0;
    private int compScore = 0;
    private int currentRoll;
    private int currentTurnTotal = 0;

    private String currentTurn = "PLAYER";
    private JTextField turnField;
    private JTextField rollField;
    private JTextField currentTurnTotalField;

    private JButton roll;
    private JButton hold;
    private JLabel compLabel;
    private JTextField compText;
    private JLabel playerLabel;
    private JTextField playerText;
    private boolean gameOver = false;

    //TimerListener to implement time delay during computer turn
    ActionListener listener = new TimerListener();
    final int DELAY = 600;
    Timer compTimer = new Timer(DELAY,listener);

    public PigFrame(){
        //to track whose turn
        turnField = new JTextField((currentTurn + "'S TURN"),JLabel.CENTER);
        turnField.setHorizontalAlignment(JTextField.CENTER);
        //pig game title
        title = new JLabel("PIG GAME",SwingConstants.CENTER);
        title.setFont(new Font("San Serif",Font.PLAIN,24));

        //track current roll
        rollField = new JTextField("Roll to begin");
        rollField.setFont(new Font("San Serif",Font.PLAIN,36));
        rollField.setHorizontalAlignment(JTextField.CENTER);
        currentTurnTotalField = new JTextField(String.valueOf(currentTurnTotal));

        //roll and hold buttons
        roll = new JButton("ROLL");
        ActionListener rollListener = new RollListener();
        roll.addActionListener(rollListener);
        hold = new JButton("HOLD");
        ActionListener holdListener = new HoldListener();
        hold.addActionListener(holdListener);

        //track score
        compLabel = new JLabel("COMPUTER SCORE");
        compText = new JTextField(String.valueOf(compScore));
        playerLabel = new JLabel("PLAYER SCORE");
        playerText = new JTextField(String.valueOf(playerScore));

        turnField.setEditable(false);
        rollField.setEditable(false);
        compText.setEditable(false);
        playerText.setEditable(false);
        currentTurnTotalField.setEditable(false);

        createTitlePanel();
        add(roll,BorderLayout.WEST);
        add(hold,BorderLayout.EAST);
        createRollPanel();
        createScorePanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);

    }

    private void createTitlePanel(){
        JPanel titlePanel = new JPanel();
        titlePanel.setLayout(new GridLayout(2,1));
        titlePanel.add(title);
        titlePanel.add(turnField);
        add(titlePanel,BorderLayout.NORTH);
    }

    private void createRollPanel(){
        JPanel rollPanel = new JPanel();
        rollPanel.setLayout(new GridLayout(2,1));
        JPanel currentTurnPanel = new JPanel();
        currentTurnPanel.setLayout(new GridLayout(1,2));
        currentTurnPanel.add(new JLabel("TURN TOTAL"));
        currentTurnPanel.add(currentTurnTotalField);
        rollPanel.add(rollField);
        rollPanel.add(currentTurnPanel);
        add(rollPanel,BorderLayout.CENTER);
    }

    private void createScorePanel(){
        JPanel scorePanel = new JPanel();
        scorePanel.setLayout(new GridLayout(2,2));
        scorePanel.add(compLabel);
        scorePanel.add(playerLabel);
        scorePanel.add(compText);
        scorePanel.add(playerText);
        add(scorePanel,BorderLayout.SOUTH);
    }

    //what happens when roll is clicked
    class RollListener implements ActionListener {
        public void actionPerformed(ActionEvent event){
            //if player's turn and game is not over roll dice
            if(currentTurn.equals("PLAYER") && !gameOver){
                currentRoll = (int) (Math.random()*6 + 1);
                rollField.setText(String.valueOf(currentRoll));

                //if roll is 1 then reset and switch to computer turn, start computer timer
                if(currentRoll == 1){
                    currentTurnTotal = 0;
                    currentTurnTotalField.setText(String.valueOf(currentTurnTotal));
                    currentTurn = "COMPUTER";
                    turnField.setText(currentTurn + "'S TURN");
                    compTimer.start();
                }
                //if not a 1, update current turn total
                else{
                    currentTurnTotal+=currentRoll;
                    currentTurnTotalField.setText(String.valueOf(currentTurnTotal));

                }
            }
        }
    }

    //what happens when hold is clicked
    class HoldListener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            //if player's turn and game is not over, add current total to player score, reset current total, switch to computer
            if(currentTurn.equals("PLAYER")&&!gameOver){
                playerScore += currentTurnTotal;
                playerText.setText(String.valueOf(playerScore));
                currentTurnTotal = 0;
                currentTurnTotalField.setText(String.valueOf(currentTurnTotal));
                //if player score is over 100 then the game is over and player has won
                if(playerScore>=100){
                    rollField.setText("PLAYER WINS");
                    gameOver = true;
                }
                //if game is still going, start computer timer
                else{
                    currentTurn = "COMPUTER";
                    turnField.setText(currentTurn + "'S TURN");
                    compTimer.start();
                }
            }
        }
    }

    //timer to delay each computer turn so player can see each turn
    class TimerListener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            if(currentTurn.equals("COMPUTER")){
                computerTurn();
            }
            //stop timer if no longer computer's turn
            else{
                compTimer.stop();
            }
        }
    }

    //computer moves
    public void computerTurn(){
        //if computer score >= 100, computer has won and game is over
        if(compScore>=100){
            gameOver=true;
            rollField.setText("COMPUTER WINS");
            gameOver=true;
            rollField.setFont(new Font("San Serif",Font.PLAIN,24));
        }
        else{
            //random number to decide if we will hold or not (holds 1/7 rolls or if the current total > 25)
            int hold = (int) (Math.random()*100);
            if((hold%7==0 && currentTurnTotal>0)|| currentTurnTotal>25){
                rollField.setText("HOLD");
                compScore += currentTurnTotal;
                currentTurnTotal = 0;
                currentTurnTotalField.setText(String.valueOf(currentTurnTotal));
                compText.setText(String.valueOf(compScore));
                //update if computer has won after holding
                if(compScore>=100){
                    gameOver=true;
                    rollField.setText("COMPUTER WINS");
                    gameOver=true;
                    rollField.setFont(new Font("San Serif",Font.PLAIN,24));
                }
                //if computer hasn't won then reset to player's turn
                currentTurn = "PLAYER";
                turnField.setText(currentTurn+"'S TURN");
            }
            else{
                currentRoll = (int) (Math.random()*6 + 1);
                rollField.setText(String.valueOf(currentRoll));

                //if computer rolls a 1, reset total and switch to player's turn
                if(currentRoll==1){
                    currentTurnTotal = 0;
                    currentTurnTotalField.setText(String.valueOf(currentTurnTotal));
                    currentTurn="PLAYER";
                    turnField.setText(currentTurn+"'S TURN");
                }
                //update turn total otherwise
                else{
                    currentTurnTotal+=currentRoll;
                    currentTurnTotalField.setText(String.valueOf(currentTurnTotal));
                }
            }

        }

    }
}
