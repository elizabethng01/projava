package edu.uchicago.gerber._05dice.pig;


import javax.swing.*;

public class PigDriver {

    public static void main(String[] args) {
        JFrame frame = new PigFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Game of Pig");
        frame.setVisible(true);

    }
}
