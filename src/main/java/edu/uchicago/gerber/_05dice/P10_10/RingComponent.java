package edu.uchicago.gerber._05dice.P10_10;

import java.awt.*;
import javax.swing.JComponent;

public class RingComponent extends JComponent {

    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));
        drawRing(g2,10,10,60,Color.blue);
        drawRing(g2,75,10,60,Color.black);
        drawRing(g2,140,10,60,Color.red);
        drawRing(g2,40,40,60,Color.yellow);
        drawRing(g2,105,40,60,Color.green);
    }

    void drawRing(Graphics g, int xLeft, int yTop, int height, Color colour){
        g.setColor(colour);
        g.drawOval(xLeft,yTop,height,height);
    }
}
