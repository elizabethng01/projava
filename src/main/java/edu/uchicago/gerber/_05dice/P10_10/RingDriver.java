package edu.uchicago.gerber._05dice.P10_10;

import edu.uchicago.gerber._05dice.P10_9.HorizontalFlagComponent;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class RingDriver {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(300,400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component = new RingComponent();
        frame.add(component);

        frame.setVisible(true);
    }
}
