package edu.uchicago.gerber._06design.E12_4;
/*
Write a program that teaches arithmetic to a young child. The program tests addition and subtraction.
In level 1, it tests only addition of numbers less than ten whose sum is less than ten.
In level 2, it tests addition of arbitrary one-digit numbers.
In level 3, it tests subtraction of one-digit numbers with a nonnegative difference.
Generate random problems and get the player’s input. The player gets up to two tries per problem.
Advance from one level to the next when the player has achieved a score of five points.
 */
import java.util.Scanner;

public class ArithmeticDriver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        boolean playGame = true;

        while(playGame) {
            System.out.println("Welcome to Math Whizz! This game has 3 levels. When you score 5 points, you level up!" +
                    "\nBut be careful, if you answer a question incorrectly twice, it's game over!" +
                    "\nHere comes the first question:");
            int level = 1;
            int score = 0;
            int incorrectCount;
            boolean cont = true;

            while (cont && score < 15) {
                incorrectCount=0;
                Question q = new Question(level);
                int answer = 0;
                System.out.println(q.getQuestion());

                while(cont) {
                    String input = in.nextLine();
                    boolean goodAns = false;
                    while (!goodAns) {
                        try {
                            answer = Integer.parseInt(input);
                            goodAns=true;
                        } catch (NumberFormatException e) {
                            System.out.println("Please enter your answer: ");
                        }
                    }
                    //if the answer is correct, break current question to start new one
                    if(answer == q.getAnswer()) {
                        score += 1;
                        System.out.println("Correct! Your score is now " + score + ".");
                        break;
                    }
                    //if there is already one incorrect answer and there is another incorrect answer, then they cannot continue
                    else if (incorrectCount==1){
                        cont = false;
                    }
                    else{
                        System.out.println("Incorrect, try again!");
                        incorrectCount+=1;
                    }
                }
                //if the score is 5 or 10, level up
                if(score==5 || score ==10){
                    level+=1;
                    System.out.println("Great job, you have advanced to the next level! You are now on level " + level);
                }

            }
            //if the game stopped because they answered incorrectly
            if (!cont) {
                System.out.println("Uh oh, you answered a question incorrectly twice. Game Over.\nWould you like to start math whizz again? y/n");
            }
            else {
                System.out.println("Congratulations! You completed math whizz! Would you like to play again? y/n");
            }
            String response=in.nextLine();
            if (response.charAt(0)=='n' || response.charAt(0)=='N') {
                playGame = false;
            }
        }
    }
}
