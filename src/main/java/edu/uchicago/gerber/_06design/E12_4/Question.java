package edu.uchicago.gerber._06design.E12_4;
/*
Write a program that teaches arithmetic to a young child. The program tests addition and subtraction.
In level 1, it tests only addition of numbers less than ten whose sum is less than ten.
In level 2, it tests addition of arbitrary one-digit numbers.
In level 3, it tests subtraction of one-digit numbers with a nonnegative difference.
Generate random problems and get the player’s input. The player gets up to two tries per problem.
Advance from one level to the next when the player has achieved a score of five points.
 */

import java.lang.Math;

public class Question {

    private String question;
    private int answer;

    //create question depending on the level
    public Question(int level){
        int a = (int) (Math.random()*10);
        int b = (int) (Math.random()*10);
        //if level 1, make sure sum is less than 10
        if(level==1){
            while(a+b>=10){
                a = (int) (Math.random()*10);
                b = (int) (Math.random()*10);
            }
        }
        if(level<3){
            question = a + "+" + b + "= ?";
            answer = a+b;
        }
        //if it is level 3, make it a subtraction problem
        else{
            question = Math.max(a,b) + "-" + Math.min(a,b) + "=?";
            answer = Math.abs(a-b);
        }
    }

    public String getQuestion(){
        return question;
    }
    public int getAnswer(){
        return answer;
    }
}
