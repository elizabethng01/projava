package edu.uchicago.gerber._06design.P12_1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

/*
Write a program that simulates a vending machine. Products can be purchased by inserting coins with a value at least
equal to the cost of the product. A user selects a product from a list of available products, adds coins, and either
gets the product or gets the coins returned. The coins are returned if insufficient money was supplied or if the product
 is sold out. The machine does not give change if too much money was added. Products can be restocked and money removed
 by an operator. Follow the design process that was described in this chapter.
Your solution should include a class VendingMachine that is not coupled with the Scanner or PrintStream classes.

vending machine - available products and coins inserted
coin - value and name
product - value and name and inventory

 */

public class VMDriver {

    public static final DecimalFormat sDecimalFormat = new DecimalFormat("$0.00");
    //method to get total amount given array list of coins
    public static double total(ArrayList<Coin> coins){
        double total = 0;
        for(Coin c: coins){
            total = total + c.getAmount();
        }
        return total;
    }
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        VendingMachine vendy = new VendingMachine();
        vendy.add(new Product("apple",1.50));
        vendy.add(new Product("banana",2.00));
        vendy.add(new Product("cherries",2.75));
        vendy.add(new Product("dragonfruit",4.50));
        vendy.add(new Product("elderberries",2.25));
        vendy.add(new Product("fig",2.00));
        vendy.add(new Product("grape",1.75));
        vendy.add(new Product("huckleberries",3.25));
        vendy.add(new Product("jackfruit",5.75));
        vendy.add(new Product("kiwi",2.50));
        vendy.add(new Product("lychees",3.50));
        vendy.add(new Product("mango",4.00));

        boolean play = true;
        while(play){
            ArrayList<Coin> curr = new ArrayList<>();
            System.out.println("\nWelcome to The Fruit Vending Machine. Here is the current selection:");
            vendy.showItems();
            System.out.println("Please insert coins (H/Q/D/N/P). No change is given. Enter X when you are done");
            boolean admin = false;
            boolean done = false;
            //loop until user is done putting in coins
            while(!done){
                //loop if input is not good
                boolean goodInput = false;
                while(!goodInput){
                    String response = in.nextLine();
                    if(response.equals("X")){
                        done = true;
                        break;
                    }
                    else if(response.equals("ABCDE12345")){
                        admin = true;
                        done = true;
                        break;
                    }
                    else if (response.equals("H")||response.equals("Q")||response.equals("D")||response.equals("N")||response.equals("P")){
                        curr.add(new Coin(response));
                        goodInput=true;
                    }
                    else{
                        System.out.println("Invalid input. Try again");
                    }
                }
            }
            if(admin) {
                System.out.println("Welcome admin. Enter R to restock or C to cash out");
                while(true){
                    String response = in.nextLine();
                    if(response.equals("R")){
                        vendy.restock();
                        System.out.println("You have successfully restocked the vending machine");
                        break;
                    }
                    else if(response.equals("C")){
                        vendy.cashOut();
                        break;
                    }
                    else{
                        System.out.println("Invalid input. Try again.");
                    }
                }
                System.out.println("Press Y to reset vending machine to user. Press any key to end");
            }
            else {
                System.out.println("\nYou entered " + sDecimalFormat.format(total(curr)) + ". Make a selection:");
                vendy.showItems();

                while (true) {
                    //if their selection is valid then try to buy item
                    String response = in.nextLine();
                    if (vendy.identifiers().contains(response)) {
                        //if the purchase is successful, add coins to vending machine bank
                        if (vendy.buy(response, total(curr))) {
                            System.out.println("\nYou have purchased 1 " + vendy.getItem(response));
                            vendy.addCoin(curr);
                            break;
                        }
                        //if purchase is not successful, return coins
                        else {
                            System.out.println("Your money is being returned");
                            for (Coin c : curr) {
                                System.out.print(c.getName()+ " ");
                            }
                            System.out.println("\n");
                            break;
                        }

                    } else {
                        System.out.println("Invalid input. Try again");
                    }
                }
                System.out.println("Press Y to insert more coins and make another selection. Press any key to end");
            }
            //play again if user inputs Y
            String response = in.nextLine();
            if (!response.equals("Y")) {
                play = false;
            }
        }
    }
}
