package edu.uchicago.gerber._06design.P12_1;

import java.util.ArrayList;

/*
Write a program that simulates a vending machine. Products can be purchased by inserting coins with a value at least
equal to the cost of the product. A user selects a product from a list of available products, adds coins, and either
gets the product or gets the coins returned. The coins are returned if insufficient money was supplied or if the product
 is sold out. The machine does not give change if too much money was added. Products can be restocked and money removed
 by an operator. Follow the design process that was described in this chapter.
Your solution should include a class VendingMachine that is not coupled with the Scanner or PrintStream classes.

vending machine - available products and coins inserted
coin - value and name
product - value and name and inventory

 */

import java.text.DecimalFormat;
import java.util.Map;
import java.util.TreeMap;

public class VendingMachine {

    public static final int NUM_ROWS = 4;
    public static final int NUM_COLS = 4;
    public static final int NUM_SLOTS = 4;
    private char[] characters = {'A','B','C','D'} ;

    public static final DecimalFormat sDecimalFormat = new DecimalFormat("$0.00");

    private ArrayList<Product> products;
    private ArrayList<Coin> money;
    private Map<String,Product> display;

    public VendingMachine(){
        products = new ArrayList<Product>();
        money = new ArrayList<Coin>();
        display = new TreeMap<>();
    }

    //restock vending machine
    public void restock(){
        for (Product product: products) {
            product.setAmount(NUM_SLOTS);
        }
    }

    //add item and give identifier
    public void add(Product item) {
        products.add(item);
        int num = (products.size()-1)%4+1;
        char letter = characters[(products.size()-1)/4];
        String identifier = letter + String.valueOf(num);
        this.display.put(identifier, item);
    }

    public void showItems(){
        System.out.println("-----------------------------------------");
        System.out.println("    ITEM               PRICE      #AVAIL");
        System.out.println("-----------------------------------------");
        for (String identifier: display.keySet()) {
            System.out.println(identifier + ": " + display.get(identifier));
        }
        System.out.println("-----------------------------------------");
    }


    public void addCoin(ArrayList<Coin> coins){
        for(Coin c: coins){
            money.add(c);
        }
    }

    //if true then add the money to vending machine, if false then return coins
    public boolean buy(String identifier, double amount){
        if(display.get(identifier).getCost()>amount){
            System.out.println("Invalid funds.");
            return false;
        }
        else{
            return (display.get(identifier).remove());
        }
    }

    //cash out for admin, password ABCDE12345
    public void cashOut(){
        double total = 0;
        for(Coin c: money){
            total += c.getAmount();
            System.out.print(c.getName()+ " ");
        }
        System.out.println("\n" + sDecimalFormat.format(total));
        money.clear();
    }

    //return set of identifiers
    public java.util.Set<String> identifiers(){
        return display.keySet();
    }

    //get name of the item at a particular identifier
    public String getItem(String identifier){
        return display.get(identifier).getName();
    }

}
