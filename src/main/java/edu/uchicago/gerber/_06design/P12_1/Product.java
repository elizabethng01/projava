package edu.uchicago.gerber._06design.P12_1;


import java.text.DecimalFormat;

public class Product {
    private String name;
    private double cost;
    //store how many available of a particular product, initialise to number of slots in machine
    private int amount = VendingMachine.NUM_SLOTS;
    public static final DecimalFormat sDecimalFormat = new DecimalFormat("$0.00");

    public Product(String name, double cost){
        this.name = name;
        this.cost = cost;
    }

    public boolean remove(){
        //if inventory is 0, then return false
        if(amount==0){
            System.out.println("There are 0 " + name + " left in stock.");
            return false;
        }
        //if there are some available, then remove and return true
        else{
            amount=amount-1;
            return true;
        }
    }

    @Override
    public String toString() {
        StringBuilder padding1 = new StringBuilder(" ");
        for (int i = 0; i < 18-name.length(); i++) {
            padding1.append(" ");
        }
        String description = name + padding1 + sDecimalFormat.format(cost);
        return description + "        " + amount;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
