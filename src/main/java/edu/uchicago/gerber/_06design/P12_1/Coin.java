package edu.uchicago.gerber._06design.P12_1;

public class Coin {
    private double amount;
    private String name;

    public Coin(String name){
        this.name = name;
        if(name.equals("H")){
            amount = 0.50;
        }
        else if(name.equals("Q")){
            amount = 0.25;
        }
        else if (name.equals("D")){
            amount = 0.10;
        }
        else if (name.equals("N")){
            amount = 0.05;
        }
        else if (name.equals("P")){
            amount = 0.01;
        }
    }

    public String getName() {
        return name;
    }

    public double getAmount(){
        return amount;
    }
}
