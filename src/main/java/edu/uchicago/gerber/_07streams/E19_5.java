package edu.uchicago.gerber._07streams;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Write a method
			public static <T> String toString(Stream<T> stream, int n)
that turns a Stream<T> into a comma-separated list of its first n elements.
 */
public class E19_5 {
    public static <T> String toString(Stream<T> stream, int n){
        String nlist = stream
                .limit(n)
                .map(a -> a.toString())
                .collect(Collectors.joining(","));

        return nlist;
    }

    public static void main(String[] args) {
        Stream<String> words = Stream.of("HELP","ME","I'M","STUCK","IN","A","COMPUTER");
        Stream<Integer> digits = Stream.of(1,2,3,5,7,9,13,15,8);
        System.out.println(toString(words,5));
        System.out.println(toString(digits,7));
    }
}
