package edu.uchicago.gerber._07streams;
/*
Write a recursive method for computing a string with the binary digits of a number. If n is even, then the last digit
is 0. If n is odd, then the last digit is 1. Recursively obtain the remaining digits.
 */
import java.util.InputMismatchException;
import java.util.Scanner;

public class E13_4 {

    public static String toBinary(int n){
        return toBinary(n,new StringBuilder());
    }

    public static String toBinary(int n, StringBuilder inBin){
        if(n==0){
            inBin.append('0');
            return inBin.reverse().toString();
        }

        else if(n==1){
            inBin.append('1');
            return inBin.reverse().toString();
        }
        else{
            inBin.append(String.valueOf(n%2));
            return toBinary((n/2),inBin);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean cont = true;

        while(cont){
            System.out.println("Enter a positive number to turn into binary: ");
            int number;
            String response = in.nextLine();
            while(true){
                try{
                    number = Integer.parseInt(response);
                    break;
                }
                catch (InputMismatchException e){
                    System.out.println("You did not enter an integer greater than 0. Please try again");
                }
            }
            System.out.println("Your number in binary is: " + toBinary(number));
            System.out.println("Would you like to enter another number? y/n");
            response = in.nextLine();
            if(response.toLowerCase().charAt(0)=='n'){
                cont = false;
            }
        }
    }
}
