package edu.uchicago.gerber._07streams;
/*
Read all words from a file into an ArrayList<String>, then turn it into a parallel stream. Use the dictionary file
words.txt provided with the book’s companion code. Use filters and the findAny method to find any palindrome that has
at least five letters, then print the word. What happens when you run the program multiple times?
 */

import java.io.File;
import java.io.IOException;
import java.util.*;

public class E19_14 {
    public static void add(ArrayList<String> words, String[] splitLine){
        for (String s: splitLine) {
            words.add(s);
        }
    }
    public static boolean palindrome(String word){
        return palindrome(word.toLowerCase(),0,word.length()-1);
    }
    public static boolean palindrome(String word, int start, int end){
        if(start>=end){
            if(Character.isLetter(word.charAt(start)) && Character.isLetter(word.charAt(end))){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            //check if the characters are letters
            if (Character.isLetter(word.charAt(start)) && Character.isLetter(word.charAt(end))){
                if(word.charAt(start)==word.charAt(end)){
                    return palindrome(word,start+1,end-1);
                }
                else{
                    return false;
                }
            }
            //if they are not letters than check the next letter in string
            else{
                if(!Character.isLetter(word.charAt(start))){
                    return palindrome(word,start+1,end);
                }
                else{
                    return palindrome(word,start,end-1);
                }
            }
        }
    }
    public static void main(String args[]) {

        String fileName = "C:\\Users\\elizabethng\\IdeaProjects\\projava\\src\\main\\java\\edu\\uchicago\\gerber\\_07streams\\warAndPeace.txt";
        ArrayList<String> words = new ArrayList<String>();
        try{
            Scanner inFile = new Scanner(new File(fileName));

            while (inFile.hasNext()){
                add(words,inFile.next().split("[ ,.]+"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        String palindrome = words
                .parallelStream()
                .filter(s -> s.length()>5)
                .filter(w -> palindrome(w))
                .findAny()
                .orElse("No palindrome found");
        System.out.println("A palindrome in War and Peace: " + palindrome);
    }
}
