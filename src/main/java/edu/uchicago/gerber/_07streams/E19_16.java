package edu.uchicago.gerber._07streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Read all words in a file and group them by the first letter (in lowercase). Print the average word length for each
initial letter. Use collect and Collectors.groupingBy.
 */
public class E19_16 {

    public static Function<String[],Double> avgLength = strings ->{
        double total=0;
        for (String word: strings) {
            total+=word.length();
        }
        return (total/(strings.length));
    };

    public static void main(String args[]) {


        String fileName = "C:\\Users\\elizabethng\\IdeaProjects\\projava\\src\\main\\java\\edu\\uchicago\\gerber\\_07streams\\someWords.txt";

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            Map<Character,List<String>> groupedWords = stream
                    .flatMap(l -> Arrays.stream(l.split("[ ,.]+")))
                    .collect(Collectors.groupingBy(c -> c.charAt(0)));

            for (Character key: groupedWords.keySet()) {
                int totalCharacters = 0;
                for (String word: groupedWords.get(key)) {
                    totalCharacters+=word.length();
                }
                System.out.println(key + ": " + ((double) totalCharacters/groupedWords.get(key).size()));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
