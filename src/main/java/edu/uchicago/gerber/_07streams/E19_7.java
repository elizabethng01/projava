package edu.uchicago.gerber._07streams;
/*
Write a lambda expression for a function that turns a string into a string made of the first letter, three periods,
and the last letter, such as "W...d". (Assume the string has at least two letters.) Then write a program that reads
words into a stream, applies the lambda expression to each element, and prints the result. Filter out any words with
fewer than two letters.
 */

import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Stream;


public class E19_7 {
    public static Function<String,String> wordFunc = w -> w.charAt(0) + "..." + w.charAt(w.length()-1);

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the text you would like to transform: ");
        String response = in.nextLine();
        Stream<String> words = Stream.of(response.split(" "));
        words.filter(w -> w.length()>2).map(wordFunc).forEach(w -> System.out.print(w + " "));
    }
}
