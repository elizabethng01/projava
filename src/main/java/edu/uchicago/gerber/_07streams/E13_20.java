package edu.uchicago.gerber._07streams;
/*
Given an integer price, list all possible ways of paying for it with $100, $20, $5, and $1 bills, using recursion.
Don’t list duplicates.
 */

import java.util.*;

public class E13_20 {
    public static void printPayBill(int n){
        Set<HashMap<Integer,Integer>> result = new HashSet<HashMap<Integer,Integer>>(payBill(n));
        System.out.println("To make $" + n + ", you can use: ");
        for (HashMap<Integer,Integer> map: result ) {
            String prefix = "";
            for (Integer key: map.keySet()) {
                System.out.print(prefix + map.get(key) + " $"+ key + " bills");
                prefix = ", ";
            }
            System.out.println();
        }
    }
    public static ArrayList<HashMap<Integer,Integer>> payBill(int n){
        ArrayList<HashMap<Integer,Integer>> result = new ArrayList<HashMap<Integer,Integer>>();
        if(n<0){
            return result;
        }
        //if n = 0, return empty hashmap
        else if(n==0){
            HashMap<Integer,Integer> temp = new HashMap<Integer,Integer>();
            result.add(temp);
            return result;
        }
        //if n = 1, we can only make it with 1 $1 bill
        else if(n==1){
            HashMap<Integer,Integer> temp = new HashMap<Integer,Integer>();
            temp.put(1,1);
            result.add(temp);
            return result;
        }
        else{
            //if n>=100, then add 1 $100 to each possible way to pay the amount - 100
            if(n>=100){
                ArrayList<HashMap<Integer,Integer>> recurs = payBill(n-100);
                for (HashMap<Integer,Integer> s :recurs) {
                    if(s.containsKey(100)){
                        s.replace(100,s.get(100)+1);
                    }
                    else{
                        s.put(100,1);
                    }
                    result.add(s);
                }

            }
            //if n>=20, then add 1 $20 to each possible way to pay the amount - 20
            if(n>=20){
                ArrayList<HashMap<Integer,Integer>> recurs = payBill(n-20);
                for (HashMap<Integer,Integer> s :recurs) {
                    if(s.containsKey(20)){
                        s.replace(20,s.get(20)+1);
                    }
                    else{
                        s.put(20,1);
                    }
                    result.add(s);
                }
            }
            //if n>=5, then add 1 $5 to each possible way to pay the amount - 5
            if(n>=5){
                ArrayList<HashMap<Integer,Integer>> recurs = payBill(n-5);
                for (HashMap<Integer,Integer> s :recurs) {
                    if(s.containsKey(5)){
                        s.replace(5,s.get(5)+1);
                    }
                    else{
                        s.put(5,1);
                    }
                    result.add(s);
                }
            }
            //if n>1, then add 1 $1 to each possible way to pay the amount - 1
            {
                ArrayList<HashMap<Integer,Integer>> recurs = payBill(n-1);
                for (HashMap<Integer,Integer> s :recurs) {
                    if(s.containsKey(1)){
                        s.replace(1,s.get(1)+1);
                    }
                    else{
                        s.put(1,1);
                    }
                    result.add(s);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean cont = true;
        while(cont) {
            System.out.println("Enter an integer to change into bills: ");

            while(true){
                String response = in.nextLine();
                try {
                    int number = Integer.parseInt(response);
                    printPayBill(number);
                    break;

                } catch (NumberFormatException e) {
                    System.out.println("Invalid input. Try again");
                }
            }
            System.out.println("Would you like to input another int? y/n");
            if(in.nextLine().toLowerCase().charAt(0)=='n'){
                cont=false;
            }
        }
    }
}
