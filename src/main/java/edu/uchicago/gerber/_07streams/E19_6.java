package edu.uchicago.gerber._07streams;
/*
The static getAvailableCurrencies method of the java.util.Currency class yields a set of Currency objects.
Turn it into a stream and transform it into a stream of the currency display names. Print them in sorted order.
 */

import java.util.Currency;
import java.util.Set;

public class E19_6 {
    public static void main(String[] args) {
        Set<Currency> currencies = Currency.getAvailableCurrencies();
        currencies.stream().map(c -> c.getDisplayName()).sorted().forEach(c->System.out.println(c));


    }
}
