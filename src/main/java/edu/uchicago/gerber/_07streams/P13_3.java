package edu.uchicago.gerber._07streams;

/*
With a longer number, you may need more than one word to remember it on a phone pad. For example, 263-346-5282 is
CODE IN JAVA. Using your work from Exercise •• P13.2, write a program that, given any number, lists all word sequences
that spell the number on a phone pad.
 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P13_3 {
    //convert num to word using starting and ending index
    public static ArrayList<String> numtoWord(String num, int start, int end){
        ArrayList<String> result = new ArrayList<>();
        if(start==end){
            if(num.charAt(start)=='2'){
                return new ArrayList<>(Arrays.asList("A","B","C"));
            }
            else if(num.charAt(start)=='3'){
                return new ArrayList<>(Arrays.asList("D","E","F"));
            }
            else if(num.charAt(start)=='4'){
                return new ArrayList<>(Arrays.asList("G","H","I"));
            }
            else if(num.charAt(start)=='5'){
                return new ArrayList<>(Arrays.asList("J","K","L"));
            }
            else if(num.charAt(start)=='6'){
                return new ArrayList<>(Arrays.asList("M","N","O"));
            }
            else if(num.charAt(start)=='7'){
                return new ArrayList<>(Arrays.asList("P","Q","R","S"));
            }
            else if(num.charAt(start)=='8'){
                return new ArrayList<>(Arrays.asList("T","U","V"));
            }
            else if(num.charAt(start)=='9'){
                return new ArrayList<>(Arrays.asList("W","X","Y","Z"));
            }
        }
        else {
            ArrayList<String> recursiveStep = numtoWord(num,start+1,end);
            if (num.charAt(start) == '2') {
                for (String s : recursiveStep) {
                    result.add("A" + s);
                    result.add("B" + s);
                    result.add("C" + s);
                }
            } else if (num.charAt(start) == '3') {
                for (String s : recursiveStep) {
                    result.add("D" + s);
                    result.add("E" + s);
                    result.add("F" + s);
                }
            } else if (num.charAt(start) == '4') {
                for (String s : recursiveStep) {
                    result.add("G" + s);
                    result.add("H" + s);
                    result.add("I" + s);
                }
            } else if (num.charAt(start) == '5') {
                for (String s : recursiveStep) {
                    result.add("J" + s);
                    result.add("K" + s);
                    result.add("L" + s);
                }

            } else if (num.charAt(start) == '6') {
                for (String s : recursiveStep) {
                    result.add("M" + s);
                    result.add("N" + s);
                    result.add("O" + s);
                }
            } else if (num.charAt(start) == '7') {
                for (String s : recursiveStep) {
                    result.add("P" + s);
                    result.add("Q" + s);
                    result.add("R" + s);
                    result.add("S" + s);
                }
            } else if (num.charAt(start) == '8') {
                for (String s :recursiveStep) {
                    result.add("T" + s);
                    result.add("U" + s);
                    result.add("V" + s);
                }
            } else if (num.charAt(start) == '9') {
                for (String s :recursiveStep) {
                    result.add("W" + s);
                    result.add("X" + s);
                    result.add("Y" + s);
                    result.add("Z" + s);
                }
            }
        }
        return result;
    }
    //convert num to word using starting and ending index and checking against dictionary of words
    public static ArrayList<String> numtoWordCheck(String num, ArrayList<String> words,int start, int end){
        ArrayList<String> possWords = numtoWord(num, start, end);
        ArrayList<String> result = new ArrayList<>();
        for (String s: possWords) {
            if (words.contains(s)){
                result.add(s);
            }
        }
        return result;
    }
    //convert num to word using starting index and length of substring and checking against dictionary of words
    public static ArrayList<String> numtoWordLength(String num, ArrayList<String> words, int start, int length){
        return numtoWordCheck(num, words, start, start+length-1);
    }
    public static void add(ArrayList<String> words, String[] splitLine){
        for (String s: splitLine) {
            words.add(s);
        }
    }
    //convert longer number to all potential sentences and checking against dictionary of words
    public static ArrayList<String> longNumToWord(String num, ArrayList<String> words){
        String cleanWord = num.replace("-","");
        return longNumToWord(cleanWord,words,0,cleanWord.length()-1);
    }
    //convert longer number to all potential sentences using starting and ending index and checking against dictionary of words
    public static ArrayList<String> longNumToWord(String num, ArrayList<String> words, int start, int end){
        ArrayList<String> result = new ArrayList<>();
        if(start>end){
            return result;
        }
        else if(start==end){
            ArrayList<String> single = numtoWordCheck(num,words,start,end);
            for (String s: single) {
                    result.add(s);
            }
        }
        else{
            for (String s:numtoWordCheck(num,words,start,end)) {
                    result.add(s);
            }

            for (int i = 1; i < end-start+1; i++) {
                ArrayList<String> wordlengthi = numtoWordLength(num,words,start,i);
                ArrayList<String> remainingi = longNumToWord(num,words,start+i,end);
                for (String s: wordlengthi) {
                    for (String str: remainingi) {
                        result.add(s + " " + str);
                    }
                }
            }
        }
        return result;

    }

    public static void main(String[] args) {
        String fileName = "C:\\Users\\elizabethng\\IdeaProjects\\projava\\src\\main\\java\\edu\\uchicago\\gerber\\_07streams\\words.txt";
        ArrayList<String> words = new ArrayList<>();
        try (Stream<String> lines = Files.lines(Paths.get(fileName))){
            words = new ArrayList<>(lines
                    .filter(w -> !w.endsWith("'s"))
                    .filter(w -> w.equals("I") || w.equals("A") || w.length()>1)
                    .map(w-> w.toUpperCase())
                    .collect(Collectors.toList()));
        } catch (IOException e) {
                e.printStackTrace();
            }
        ArrayList<String> codeInJava = longNumToWord("263-346-5282",words);
        for (String s: codeInJava) {
            System.out.println(s);
        }
    }
}
