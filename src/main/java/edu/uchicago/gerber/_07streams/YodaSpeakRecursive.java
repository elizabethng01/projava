package edu.uchicago.gerber._07streams;

import java.util.Scanner;

public class YodaSpeakRecursive {

    //recursively speak in yoda using starting and ending index and string array
    public static String recursiveYodaSpeak(String[] sentence, int start, int end){
        if(start==end){
            return sentence[start];
        }
        else{
            return sentence[end] + " " + recursiveYodaSpeak(sentence,start,end-1);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean cont = true;
        while(cont){
            System.out.println("Input a sentence to translate into Yoda Speak:");
            String sentence = in.nextLine();
            String[] split = sentence.split(" ");
            System.out.println(recursiveYodaSpeak(split,0,split.length-1));
            System.out.println("Would you like to input another sentence? y/n");
            String response = in.nextLine();
            if(response.toLowerCase().charAt(0)=='n'){
                cont = false;
            }
        }
    }
}
