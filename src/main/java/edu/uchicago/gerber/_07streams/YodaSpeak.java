package edu.uchicago.gerber._07streams;

import java.util.Scanner;

public class YodaSpeak {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean cont = true;
        while(cont){
            System.out.println("Input a sentence to translate into Yoda Speak:");
            String sentence = in.nextLine();
            String[] split = sentence.split(" ");
            //iteratively print out sentence going backwards and decrementing index
            for (int i = split.length-1; i >= 0; i--) {
                System.out.print(split[i] + " ");
            }
            System.out.println("\nWould you like to input another sentence? y/n");
            String response = in.nextLine();
            if(response.toLowerCase().charAt(0)=='n'){
                cont = false;
            }
        }


    }
}
